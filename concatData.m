%% --- Script to concatenate and plot all "photonfit" output

% Directory to find all .dat files
ds = datastore('/home/keenan/Documents/scint_slab_depth/photonfit/*.dat');

% Specifying parameters for the photonfit function.
det_dim_pix = [80 80];
det_dim_mm = [200 200];

% Initial conditions for loop.
previousEvent = 0;
previousOutput = table;

% Loop takes each file in the directory, creates an output table for each
% with photonfit, then concatenates them into a large singular output.
for i = 1:numel(ds.Files)

    filename = ds.Files{i,1};

    output = photonfit (filename, det_dim_pix, det_dim_mm);
    
    % Change the event number to pick up from previous output.
    output.event = output.event + previousEvent;
    previousEvent = max(output.event);
    
    % Concatenate new table with previous.
    previousOutput = vertcat(previousOutput,output);

end

% Save output table.
writetable(previousOutput, 'finalOutput.txt')

% Scatter plots and histograms for the output table.
locationError (previousOutput)