%% Line graphs for presentation. Error vs. Compton scatters.

A = zeros(10, 6);
for n = 0:9
    rows = finalOutput(:,7) == n;
    cols = [7:8 13:16];
    a = finalOutput(rows, cols);
    A(n+1, 1) = n;
    A(n+1, 2) = mean(a(:,2));
    A(n+1, 3) = mean(a(:,3));
    A(n+1, 4) = mean(a(:,4));
    A(n+1, 5) = mean(a(:,5));
    A(n+1, 6) = mean(a(:,6));
end

figure(1)
plot(A(:,1), A(:,[3:6]))
title('Displacement Error - LaBr3:Ce Nanocomposite')
xlabel('Number of Compton Events')
ylabel('Displacement (mm)')
legend('Del X', 'Del Y', 'Del Z', 'Del D','Location','northwest')

figure(2)
bar(A(:,1), (A(:,2)))
title('Fitting Function MSE - LaBr3:Ce Nanocomposite')
xlabel('Number of Compton Events')
ylabel('Mean Squared Error')