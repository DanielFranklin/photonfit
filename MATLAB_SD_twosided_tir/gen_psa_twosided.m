function fh = gen_psa_twosided (atten_length, critical_angle, slab_thickness)

fh = @(params, grid_coords) pointsourceattenuative_twosided (params, grid_coords, atten_length, critical_angle, slab_thickness);
