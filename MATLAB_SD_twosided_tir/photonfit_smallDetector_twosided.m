% det_dim_pix and det_dim_mm are 2-element vectors of the detector
% dimensions in pixels and mm, respectively.
% 
% For the sim we are currently using, the slab is 2 cm square (i.e.  20
% mm).  It seems that about 10 pixels is reasonable. So call this as
% photonfit ('Hits.dat', [10 10], [20 20]);
%
% The axes of the subfigures are linked together; move one and you move the
% other too.
%
% fastmode skips drawing figures. Active/deactive as 1/0.
%
% Added in arguments for attenuation length and slab thickness of the
% scintillator (both in mm). Must include total thickness of epoxy (normally 0.3mm).

function [output] = photonfit_smallDetector_twosided (filename, det_dim_pix, det_dim_mm, atten_length, critical_angle_degrees, slab_thickness, yield, fastmode)

% Load data file.
delimiterIn = ',';
headerlinesIn = 1;
dataStructure = importdata (filename, delimiterIn, headerlinesIn);

% Generate an XY grid representing the bounds of the detector.
step = det_dim_mm ./ det_dim_pix;

c{1} = (-(det_dim_pix(1) - 1) / 2 : (det_dim_pix(1) - 1) / 2) * step (1);
c{2} = (-(det_dim_pix(2) - 1) / 2 : (det_dim_pix(2) - 1) / 2) * step (2);

[X, Y] = meshgrid (c{1}, c{2});

grid_coords = zeros (det_dim_pix (2), det_dim_pix (1), 2);
grid_coords (:, :, 1) = X; 
grid_coords (:, :, 2) = Y;

% Filter out the optical photons from the main dataset, include only rows
% that specify their position of detection and the event ID. (Column 5 is
% parent ID and column 3 is pixel ID.)
primary_secondary_rows_back = find (dataStructure.data (:,8) == slab_thickness/2 | ...
    dataStructure.data (:,5) == 0);

primary_secondary_rows_front = find (dataStructure.data (:,8) == -slab_thickness/2 | ...
    dataStructure.data (:,5) == 0);

% Keep only event and parent ID's and X,Y,Z positions.
allData_back = dataStructure.data(primary_secondary_rows_back, [4:5 8:10]); 
allData_front = dataStructure.data(primary_secondary_rows_front, [4:5 8:10]);

% Split data up into photons hitting the front and the back detectors.
optical_rows_back = find (dataStructure.data (:,5) == 1 & dataStructure.data (:,8) == slab_thickness/2);
opticalHits_back = dataStructure.data (optical_rows_back, [4 8:11]);

optical_rows_front = find (dataStructure.data (:,5) == 1 & dataStructure.data (:,8) == -slab_thickness/2);
opticalHits_front = dataStructure.data (optical_rows_front, [4 8:11]);

% Find max optical hits (front or back)
max_optical_hits = max(max(opticalHits_back (:,1)), max(opticalHits_front (:,1)));

% Create a 3D matrix Z where each slice will be filled with the hit data
% from a single event.
Z_back = zeros (det_dim_pix (2), det_dim_pix (1), max_optical_hits + 1);
Z_front = Z_back;

Z = [Z_back Z_front];

% create an array that will be populated (later) with output data.
output = zeros(max(opticalHits_back(:,1)) + 1, 19);


%% --- Function Fitting ---
% Options an input parameters for lsqcurvefit
if fastmode == 1
    options = optimoptions('lsqcurvefit','Algorithm','levenberg-marquardt', 'MaxIter', 1000, 'MaxFunEvals', 5000, 'TolFun', 1e-20, 'TolX', 1e-20);
else
    options = optimoptions('lsqcurvefit','Algorithm','levenberg-marquardt', 'Display', 'iter-detailed', 'MaxIter', 1000, 'MaxFunEvals', 5000, 'TolFun', 1e-20, 'TolX', 1e-20);
end

lb = [];
ub = [];

objfunc = gen_psa_twosided (atten_length, critical_angle_degrees, slab_thickness);

for event = 1:max(opticalHits_back(:,1)) + 1
	disp (event)
	% Filtering optical hits by event ID.
	opticalFilt_back = opticalHits_back (ismember (opticalHits_back(:, 1), event - 1), :); 
    opticalFilt_front = opticalHits_front (ismember (opticalHits_front(:, 1), event - 1), :); 
    
    if isempty(opticalFilt_back) || isempty(opticalFilt_front)
        continue          
    end
	
	% Populating Z with optical hits on the XY plane. Each slice of Z is a
	% different event. Then plot. Bin centers need to be specified to
	% correctly centre histogram.
%    c = {[], []};
%    c{1} = -((det_dim_pix - 1)/2):1:((det_dim_pix - 1)/2);
%    c{2} = c{1};
%    step
    
%    size(c{1})
%    size(c{2})
    
	Z_back = hist3 ([opticalFilt_back(:, 4) opticalFilt_back(:, 3)], 'Ctrs', c);       
    Z_front = hist3 ([opticalFilt_front(:, 4) opticalFilt_front(:, 3)], 'Ctrs', c);

%     size(Z_back)
%     size(Z_front)
    
    Z (:, :, event) = [Z_back' Z_front'];
        
    if fastmode == 0
         % set all plot interpreters to latex - changes axes, legend and
         % tick labels.
        set(groot,'defaulttextinterpreter','latex');  
        set(groot, 'defaultAxesTickLabelInterpreter','latex');  
        set(groot, 'defaultLegendInterpreter','latex');
        
         % note row and column swapped here in hist3 to be consistant with
         % X and Y definitions. For export into Latex histogram2 may work
         % better.
%         ax1 = subplot (2, 2, 1);
        % Need to define bin edges for histogram2, since we can't use
        % centers.
        Xbinedges = (-(det_dim_pix(1)) / 2 : (det_dim_pix(1)) / 2) * step (1);
        Ybinedges = (-(det_dim_pix(2)) / 2 : (det_dim_pix(2)) / 2) * step (2);
        
        figure (1)
        histogram2(opticalFilt_front(:, 4),opticalFilt_front(:, 3),'XBinEdges',Xbinedges, ...
            'YBinEdges',Ybinedges,'FaceColor','flat')

%         hist3 ([opticalFilt_front(:, 4) opticalFilt_front(:, 3)], 'Ctrs', c, ...
%             'FaceColor','interp','CDataMode','auto')
        
        xlabel('x Position (mm)')
        ylabel('y Position (mm)')
        zlabel('Intensity (photons/mm$^{3}$)')
        axis tight
           
%         ax2 = subplot (2, 2, 2);
        figure (2)
        histogram2(opticalFilt_back(:, 4),opticalFilt_back(:, 3),'XBinEdges',Xbinedges, ...
            'YBinEdges',Ybinedges,'FaceColor','flat')
        
%         hist3 ([opticalFilt_back(:, 4) opticalFilt_back(:, 3)], 'Ctrs', c, ...
%             'FaceColor','interp','CDataMode','auto')

        xlabel('x Position (mm)')
        ylabel('y Position (mm)')
        zlabel('Intensity (photons/mm$^{3}$)')
        axis tight
    end

    % Starting guess of x, y, z = (0, 0, 25), zero attenuation and total number
    % of photons in the burst is assumed to be the total number seen on the
    % detector (we lose a few but should be fairly close).  See
    % pointsourceattenuative.m for details.
        
    % The initial parameters have been changed to randomized values to
    % avoid local minima. Positions have been confined to an interval
    % based on the physical size of the scintillator. For interval
    % (a,b), r_i = a + (a - b).*rand(N).
    % for loop calculates the parameters i times, then compares the resnorm of
    % each and takes the values corresponding to the lowest.
    max_back = max(Z_back(:));
    max_front = max(Z_front(:));
    
    if max_back > max_front
        [row, col] = find(ismember(Z_back, max(Z_back(:))));
    else
        [row, col] = find(ismember(Z_front, max(Z_front(:))));
    end
    
    [num_peaks] = size(row);
    
    params = [];
    resnorm = [];
    residual = [];
    exitflag = [];
    
    for i = 1:5
        
        if num_peaks(1) > 1
            x_i = -10 + (10 + 10).*rand(1);
            y_i = -10 + (10 + 10).*rand(1);
        else
            x_i = X(row, col);
            y_i = Y(row, col); 
        end

        z_i = 2.5 + (slab_thickness - 5).*rand(1);

        %initial_params = [x_i, y_i, z_i, 0, sum(sum (Z (:, :, event)))];
        initial_params = [x_i, y_i, z_i, 0, yield];

        [calc_params, calc_resnorm, calc_residual, calc_exitflag] = lsqcurvefit (objfunc, ...
            initial_params, grid_coords, Z (:, :, event), lb, ub, options);

        if isempty(params)
            params = calc_params;
            resnorm = calc_resnorm;
            residual = calc_residual;
            exitflag = calc_exitflag;

        elseif calc_resnorm < resnorm 
            params = calc_params;
            resnorm = calc_resnorm;
            residual = calc_residual;
            exitflag = calc_exitflag;
        end
    end
        
    % To test if the paramaters lie within the volume of the
    % scintillator.
%    if params(1) < -10 || params(1) > 10 || params(2) < -10 || params(2) > 10 || params(3) < 0.15 || params(3) > slab_thickness - 0.15
%        continue          
%    end
	
	% Solution to the gaussian (with calculated parameters) used in the
	% calculation of the MSE. (**function no longer gaussian**)
%	 G = D2GaussFunctionRot(params,grid_coords);
	G = pointsourceattenuative_twosided (params, grid_coords, atten_length, critical_angle_degrees, slab_thickness);

%     size (G)
    
    if fastmode == 0
%         ax3 = subplot (2, 2, 3);
        figure (3)
        mesh (X, Y, G(:,(det_dim_pix(1) + 1) : (2 * det_dim_pix(1))));
        xlabel('x Position (mm)')
        ylabel('y Position (mm)')
        zlabel('Intensity (photons/mm$^{3}$)')
        xlim([-((det_dim_pix(1)) / 2) ((det_dim_pix(1)) / 2)])
        axis tight
            
%         ax3 = subplot (2, 2, 4);
        figure (4)
        mesh (X, Y, G(:,1:det_dim_pix(1)));
        xlabel('x Position (mm)')
        ylabel('y Position (mm)')
        zlabel('Intensity (photons/mm$^{3}$)')
        axis tight
            
        disp (sprintf ('Estimated x, y, z: %f %f %f', params (1), params (2), params (3)));
        disp (sprintf ('Estimated yield: %f photons/primary (this is probably dodgy)', params (5)));
    end
 
%% --- Plotting Primaries and Secondaries ---  
    
    % Filter the data by event #.
    allDataFilt_back = allData_back (ismember (allData_back(:, 1), event - 1), :);
    allDataFilt_front = allData_front (ismember (allData_front(:, 1), event - 1), :);
    
    if isempty(allDataFilt_back) || isempty(allDataFilt_front)
        continue
    end
    
    primary_rows_back = find (allDataFilt_back(:, 2) == 0);
    secondary_rows_back = find (allDataFilt_back(:, 2) == 1);
    
    primary_rows_front = find (allDataFilt_front(:, 2) == 0);
    secondary_rows_front = find (allDataFilt_front(:, 2) == 1);
        
%     max_rays_back = min(20, size(secondary_rows_back ,1));
%     max_rays_front = min(20, size(secondary_rows_front ,1));
        
    % Condition to filter out cases that have secondary data with no
    % primary data (cases which may interact with the epoxy orfastmode
    % meltmount. Also filter out cases with a low number of optical
    % photons.
    [num_optical_back] = size(secondary_rows_back);
    [num_optical_front] = size(secondary_rows_front);
    
    if isempty(primary_rows_back) && isempty(primary_rows_front)
        continue
%     elseif num_optical_back(1) < 100 || num_optical_front(1) < 100
%         continue
    end

    % Iterate through all primaries that we can find (i.e. where parent ID = 0)
    if fastmode == 0 
        plot_PS (primary_rows_back, allDataFilt_back, det_dim_mm, slab_thickness) % secondaries for back det.
        hold on
        
        plot_PS (primary_rows_front, allDataFilt_front, det_dim_mm, slab_thickness) % secondaries for front det.
        hold off
    end
    
%     hlink1 = linkprop ([ax1, ax2, ax3, ax4], {'cameraposition','cameraupvector'});
% 	rotate3d on;
        
%     hlink2 = linkprop ([ax3, ax4], {'cameraposition','cameraupvector'});
% 	rotate3d on;

    params(3) = ((slab_thickness - 0.3)/2) - params(3); % To change to GATE coords. 
        
    % Fill output matrix with event #, calculated values from
    % lscurvefit, # of compton scatters, mean squared error and
    % exitflag from lsqcurvefit (to make sure an answer is reached.
    output(event,1) = event;
    output(event,2:6) = params(1:5);
    output(event,7) = opticalFilt_back(end,5);
    output(event,8) = immse(Z(:,:,event), G);
    output(event,9) = resnorm;
    output(event,10) = exitflag;

    % Adding x,y,z position of the first point of interaction of
    % primary with scintillator (x0, y0, z0).  
    output(event,11:13) = [(allDataFilt_back (primary_rows_back (1), 3)), (allDataFilt_back (primary_rows_back (1), 4)), ...
        (allDataFilt_back (primary_rows_back (1), 5))];
       
    %Then add error between calculated values and actual position
    %(del_x, del_y, del_z). Abs. values removed.
    output(event,14:16) = [(output(event,2) - output(event,13)), (output(event,3) - output(event,12)), ...
    (output(event,4) - output(event,11))];
        
    % and total error (del_d).
    output(event,17) = sqrt(output(event,14).^2 + output(event,15).^2 + output(event,16).^2);
    
    % To count the total number of photons on each detector surface.
    output(event, 18) = num_optical_back(1);
    output(event, 19) = num_optical_front(1);
            

    if fastmode == 0
        pause;
        clf(1); clf(2); clf(3); clf(4); clf(5); clf(6); % clear all figures.
    end 
    
        
end

 close all;

%% --- Output result in a table ---

% Convert output into a table for data cleaning.
output = array2table(output,...
    'VariableNames',{'event', 'x', 'y', 'z', 'attenuation', 'lightYield', 'compton', 'error', 'resnorm', ...
    'exitflag', 'z0', 'y0', 'x0', 'Del_x', 'Del_y', 'Del_z', 'Del_d', 'Num_phot_back', 'Num_phot_front'})

% to cut out zero entries in the data set
output(output.event == 0,:) = [];
end
