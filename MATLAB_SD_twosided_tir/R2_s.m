function [R2, s] = R2_s (y, yest, npar)
	residual = y - yest;
	N = max (size (residual));
	SSresid = sum (residual .^ 2);
	SStotal = (length (y) - 1) * var (y);
	R2 = 1 - SSresid / SStotal;

	if N > npar
		s = sqrt (SSresid / (N - npar));
	else
		stderr = 2;
		fprintf (stderr, 'WARNING: you need at least %s samples to calculate a meaningful s\n', N + 1);
	end
end
