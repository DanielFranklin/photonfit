%% Photonfit (memory saver)
% det_dim_pix and det_dim_mm are 2-element vectors of the detector
% dimensions in pixels and mm, respectively.
% 
% For the sim we are currently using, the slab is 2 cm square (i.e.  20
% mm).  It seems that about 10 pixels is reasonable. So call this as
% photonfit ('Hits.dat', [10 10], [20 20]);
%
% The axes of the subfigures are linked together; move one and you move the
% other too.
%
% fastmode skips drawing figures. Active/deactive as 1/0.
%
% Added in arguments for attenuation length and slab thickness of the
% scintillator (both in mm). Must include total thickness of epoxy (normally 0.3mm).

function [output] = photonfit_SD_twosided_light (filename, det_dim_pix, det_dim_mm, atten_length, slab_thickness, fastmode)
%% --- Initial Parameters and variable memory allocation ---
% Generate an XY grid representing the bounds of the detector.
step = det_dim_mm ./ (det_dim_pix - 1);

[X, Y] = meshgrid((0 : step(1) : det_dim_mm(1)) - det_dim_mm(1) / 2, ...
    (0 : step(2) : det_dim_mm(2)) - det_dim_mm (2) / 2);

grid_coords = zeros (det_dim_pix (1), det_dim_pix (2), 2);
grid_coords (:, :, 1) = X; 
grid_coords (:, :, 2) = Y;

% Create a 3D matrix Z where each slice will be filled with the hit data
% from a single event.
Z_back = zeros (det_dim_pix (1), det_dim_pix (2));
Z_front = zeros (det_dim_pix (1), det_dim_pix (2));
Z = [Z_back Z_front];

% create an array that will be populated (later) with output data.
output = zeros(200000, 19);

% Options and input parameters for lsqcurvefit
if fastmode == 1
    options = optimoptions('lsqcurvefit','Algorithm','levenberg-marquardt', 'MaxIter', 1000, 'MaxFunEvals', 5000, 'TolFun', 1e-20, 'TolX', 1e-20);
else
    options = optimoptions('lsqcurvefit','Algorithm','levenberg-marquardt', 'Display', 'iter-detailed', 'MaxIter', 1000, 'MaxFunEvals', 5000, 'TolFun', 1e-20, 'TolX', 1e-20);
end

lb = [];
ub = [];

objfunc = gen_psa_twosided (atten_length, slab_thickness);

% Initial arguments for event group function.
position = 1;
status = 0;

%% --- Process Events - Fitting Function ---
while status ~=1;
    % eventOutput is primary and secondary entries with common eventID.
    [eventOutput, nextRow, status] = event_group(filename, position);
    
    eventID = eventOutput(1,4) + 1; % Add 1 to start at event 1, not 0.

    % Split data up into photons hitting the front and the back detectors.
    % Separate primaries and secondaries.
    optical_rows_back = eventOutput (:,5) == 1 & eventOutput (:,8) == slab_thickness/2;
    opticalHits_back = eventOutput (optical_rows_back, :);
    
    optical_rows_front = eventOutput (:,5) == 1 & eventOutput (:,8) == -slab_thickness/2;
    opticalHits_front = eventOutput (optical_rows_front, :);
    
    gamma_rows = eventOutput (:,5) == 0;
    
    PS_rows_back = gamma_rows | optical_rows_back; 
    PS_rows_front = gamma_rows | optical_rows_front;
    
    PSHits_back = eventOutput (PS_rows_back, :);
    PSHits_front = eventOutput (PS_rows_front, :);
    
    % Condition to filter out cases that have secondary data with no
    % primary data (cases which may interact with the epoxy meltmount. Or
    % primary data with no secondary (non-scintillation interaction).
    if isempty(gamma_rows)
        position = nextRow + position - 1;
        continue
    elseif isempty(opticalHits_back) || isempty(opticalHits_front)
        position = nextRow + position - 1;
        continue          
    end
    
    % Populating Z with optical hits on the XY plane. Each slice of Z is a
	% different event. Then plot. Bin centers need to be specified to
	% correctly centre histogram.
    c = {[], []};
    c{1} = -((det_dim_pix - 1)/2):1:((det_dim_pix - 1)/2);
    c{2} = c{1};
    
	Z_back = hist3 ([opticalHits_back(:, 9) opticalHits_back(:, 10)], 'Ctrs', c);       
    Z_front = hist3 ([opticalHits_front(:, 9) opticalHits_front(:, 10)], 'Ctrs', c);
        
    Z (:, :) = [Z_back Z_front];
        
    if fastmode == 0
        ax1 = subplot (3, 2, 1);
        mesh (X, Y, Z_front);
        axis tight;
           
        ax2 = subplot (3, 2, 2);
        mesh (X, Y, Z_back);
        axis tight;
    end
    
    % The initial parameters have been changed to randomized values to
    % avoid local minima. Positions have been confined to an interval
    % based on the physical size of the scintillator. For interval
    % (a,b), r_i = a + (a - b).*rand(N).
    % for loop calculates the parameters i times, then compares the resnorm of
    % each and takes the values corresponding to the lowest.
    max_back = max(Z_back(:));
    max_front = max(Z_front(:));
    
    if max_back > max_front
        [row, col] = find(ismember(Z_back, max(Z_back(:))));
    else
        [row, col] = find(ismember(Z_front, max(Z_front(:))));
    end
    
    [num_peaks] = size(row);
    
    params = [];
    resnorm = [];
    residual = [];
    exitflag = [];
    
    for i = 1:5
        
        if num_peaks(1) > 1
            x_i = -10 + (10 + 10).*rand(1);
            y_i = -10 + (10 + 10).*rand(1);
        else
            x_i = X(row, col);
            y_i = Y(row, col); 
        end

        z_i = 2.5 + (slab_thickness - 5).*rand(1);

        initial_params = [x_i, y_i, z_i, 0, sum(sum (Z (:, :)))];

        [calc_params, calc_resnorm, calc_residual, calc_exitflag] = lsqcurvefit (objfunc, ...
            initial_params, grid_coords, Z (:, :), lb, ub, options);

        if isempty(params);
            params = calc_params;
            resnorm = calc_resnorm;
            residual = calc_residual;
            exitflag = calc_exitflag;

        elseif calc_resnorm < resnorm; 
            params = calc_params;
            resnorm = calc_resnorm;
            residual = calc_residual;
            exitflag = calc_exitflag;
        end
    end
        
    % To test if the paramaters lie within the volume of the
    % scintillator.
    if params(1) < -10 || params(1) > 10 || params(2) < -10 || params(2) > 10 || params(3) < 0.15 || params(3) > slab_thickness - 0.15
        position = nextRow + position - 1;
        continue
    end

	% calculation of the MSE.
	G = pointsourceattenuative_twosided (params, grid_coords, atten_length, slab_thickness);

    if fastmode == 0
        ax3 = subplot (3, 2, 3);
        mesh (X, Y, G(:,21:40));
        axis tight;
            
        ax3 = subplot (3, 2, 4);
        mesh (X, Y, G(:,1:20));
        axis tight;
            
        %disp (sprintf ('Estimated x, y, z: %f %f %f', params (1), params (2), params (3)));
        %disp (sprintf ('Estimated yield: %f photons/primary (this is probably dodgy)', params (5)));
    end
    
    disp(eventID)
    %% --- Plotting Primaries and Secondaries ---     
    
    max_rays_back = min(20, size(opticalHits_back ,1));
    max_rays_front = min(20, size(opticalHits_front ,1));
    
    primary_rows_back = find(PSHits_back (:,5) == 0);
    primary_rows_front = find(PSHits_front (:,5) == 0);
    
     % Iterate through all primaries that we can find (i.e. where parent ID = 0)
    if fastmode == 0 
        plot_PS_light (primary_rows_back, max_rays_back, PSHits_back, det_dim_mm, slab_thickness) % secondaries for back det.
        
        hold on
        
        plot_PS_light (primary_rows_front, max_rays_front, PSHits_front, det_dim_mm, slab_thickness) % secondaries for front det.
    end
    
    if fastmode == 0
        pause;
    end 
    clf;
    
    %% --- Print Output ---
    params(3) = (slab_thickness/2) - params(3); % To change to GATE coords. 
    comptons = max(opticalHits_back(end,11), opticalHits_front(end,5)); 
    
    % Fill output matrix with event #, calculated values from
    % lscurvefit, # of compton scatters, mean squared error and
    % exitflag from lsqcurvefit (to make sure an answer is reached.
    output(eventID,1) = eventID;
    output(eventID,2:6) = params(1:5);
    output(eventID,7) = comptons;
    output(eventID,8) = immse(Z(:,:), G);
    output(eventID,9) = resnorm;
    output(eventID,10) = exitflag;
   
    % Adding x,y,z position of the first point of interaction of
    % primary with scintillator (x0, y0, z0).  
    output(eventID,11:13) = [(eventOutput (1, 8)), (eventOutput (1, 9)), (eventOutput (1, 10))];
       
    %Then add error between calculated values and actual position
    %(del_x, del_y, del_z). Abs. values removed.
    output(eventID,14:16) = [(output(eventID,2) - output(eventID,13)), (output(eventID,3) - output(eventID,12)), ...
    (output(eventID,4) - output(eventID,11))];
        
    % and total error (del_d).
    output(eventID,17) = sqrt(output(eventID,14).^2 + output(eventID,15).^2 + output(eventID,16).^2);
    
    % To count the total number of photons on each detector surface.    
    [num_optical_back] = size(opticalHits_back);
    [num_optical_front] = size(opticalHits_front);
    
    output(eventID, 18) = num_optical_back(1);
    output(eventID, 19) = num_optical_front(1);
    
    % Go to next event (input for event_group function).
    position = nextRow + position - 1;
    
end
%% --- Output result in a table ---

% Convert output into a table for data cleaning.
output = array2table(output,...
    'VariableNames',{'event', 'x', 'y', 'z', 'attenuation', 'lightYield', 'compton', 'error', 'resnorm', ...
    'exitflag', 'z0', 'y0', 'x0', 'Del_x', 'Del_y', 'Del_z', 'Del_d', 'Num_phot_back', 'Num_phot_front'})

% to cut out zero entries in the data set
output(output.event == 0,:) = [];
end