
function plot_PS_light (primary_rows, max_rays, Data, det_dim_mm, slab_thickness)

    x0 = -slab_thickness/2;
    y0 = 0;
    z0 = 0;
    
    colour = ['g', 'b', 'c', 'm', 'y', 'k', 'w'];
    ncolours = length (colour);

for np = 1 : max (size (primary_rows))

    px0 = x0;
    py0 = y0;
    pz0 = z0;

    x0 = Data (primary_rows (np), 8); % posX
    y0 = Data (primary_rows (np), 9); % posY
    z0 = Data (primary_rows (np), 10); % posZ
    % Get line of first optical event
    optical = primary_rows (np) + 1;
    rays = 1;

    % only run if optical row is within the matrix size (to prevent
    % indexing errors for primaries without secondary photons.
    if optical <= size(Data,1)

        figure(1);
        ax3 = subplot (3, 2, 5);
        hold on

        % Column 5 is parent ID (0 for primary gamma rays and 1 for optical
        % photons). So while parent ID = 1 (optical) and event ID = current_event_id
        % (i.e. the same event) and rays are < 20 (to speed up processing),
        % then plot X,Y,Z of each hit, then increment to next optical photon
        % (up to 19 rays).
        while (Data (optical, 5) == 1) && (rays < max_rays)
            plot3 ([x0; Data(optical, 8)], [y0; Data(optical,9)], ...
                [z0; Data(optical,10)], sprintf ('%c-*', colour (1 + mod (np, ncolours))));
            optical = optical + 1;
            rays = rays + 1;
        end        
        plot3 ([px0; x0], [py0; y0], [pz0; z0], 'r-+');
        axis equal;
        axis ([-slab_thickness/2, slab_thickness/2, -det_dim_mm(1)/2, det_dim_mm(1)/2, -det_dim_mm(2)/2, det_dim_mm(2)]);

        figure (1);
        ax4 = subplot (3, 2, 6);
        hold on
        plot3 ([px0; x0], [py0; y0], [pz0; z0], 'r-+');
        axis equal;
        axis ([-slab_thickness/2, slab_thickness/2, -det_dim_mm(1)/2, det_dim_mm(1)/2, -det_dim_mm(2)/2, det_dim_mm(2)]);

    end

end
end