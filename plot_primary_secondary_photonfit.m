function plot_primary_secondary_photonfit (event, data)

max_rays = 20;

colour = ['g', 'b', 'c', 'm', 'y', 'k', 'w'];
ncolours = length (colour);

figure(1);
clf;
hold on;

figure(2);
clf;
hold on;

current_event_id = event; % column 4 is event ID

x0 = 0;
y0 = 0;
z0 = 0;

% Iterate through all primaries that we can find (i.e. where parent ID = 0)
for np = 1 : max (size (all_primary_rows))
	prev_event_id = current_event_id;
	current_event_id = Hits (all_primary_rows (np), 4);
	
	if (current_event_id ~= prev_event_id)
		fprintf ('New hit: press enter to continue\n');
		pause;

		figure (2);
		clf;
		hold on;
		
		figure (3);
		clf;
		hold on;

		x0 = 0; % why is this written twice?
		y0 = 0;
		z0 = 0;
	end
	
	px0 = x0;
	py0 = y0;
	pz0 = z0;

	x0 = Hits (all_primary_rows (np), 8); % Column 8 is posX
	y0 = Hits (all_primary_rows (np), 9); % Column 9 is posY
	z0 = Hits (all_primary_rows (np), 10); % Column 10 is posZ

% Get line of first optical event

	optical = all_primary_rows (np) + 1;
	rays = 0;
	
	figure(2);

    % Column 5 is parent ID (0 for primary gamma rays and 1 for optical
    % photons). So while parent ID = 1 (optical) and event ID = current_event_id
    % (i.e. the same event) and rays are < 20 (to speed up processing),
    % then plot X,Y,Z of each hit, then increment to next optical photon
    % (up to 19 rays).
	while (Hits (optical, 5) == 1) && (Hits (optical, 4) == current_event_id) && (rays < max_rays)
		plot3 ([x0; Hits(optical, 8)], [y0; Hits(optical,9)], [z0; Hits(optical,10)], sprintf ('%c-*', colour (1 + mod (np, ncolours))));
		optical = optical + 1;
		rays = rays + 1;
	end
	
	plot3 ([px0; x0], [py0; y0], [pz0; z0], 'r-+');
	axis ([100, 151, -100, 100, -100, 100]);

	figure (3);

	plot3 ([px0; x0], [py0; y0], [pz0; z0], 'r-+');
	axis ([100, 151, -100, 100, -100, 100]);
end

hold off;
