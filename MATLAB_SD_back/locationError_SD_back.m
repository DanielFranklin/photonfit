
% Function to return a histogram of primary particel first hit location
% error, as calculated by the photonfit function. Then a scatter plot of
% error in the x,y,z directions with respect to the primary's distance from
% the detector.

function locationError_SD(inputTable, slab_thickness)
        
% arrange into sets depending on # of compton scatters.

for c = 0:max(inputTable.compton);
    rows = inputTable.compton == c;
    vars = {'z0','Del_x','Del_y','Del_z','Del_d'};
    num_compton{c+1} = inputTable(rows,vars);
    
    h(1) = figure(1)
    % Histogram of total distance error of estimated gamma location, for
    % varying number of compton scatters.
    edges = 0:0.5:20;
    histogram(num_compton{1,c+1}.Del_d, edges);
    title(['\Delta D Histogram, compton scatters:' num2str(c)]);
    xlim([0 20]);
    xlabel('\Delta D (mm)');
    ylabel('Frequency');

    h(2) = figure(2)
    % Scatter plots for error in x,y,z position and total distance (d) vs.
    % distance from detector (Z), for varying number of comton scatters.
    subplot (2, 2, 1)
    scatter(num_compton{1,c+1}.z0, num_compton{1,c+1}.Del_x, '.');
    axis ([-slab_thickness/2 slab_thickness/2 -20 20]);
    title('\Delta X vs. Z');
    xlabel('Z location (mm)');
    ylabel('\Delta X (mm)');

    subplot (2, 2, 2)
    scatter(num_compton{1,c+1}.z0, num_compton{1,c+1}.Del_y, '.');
    axis ([-slab_thickness/2 slab_thickness/2 -20 20]);
    title('\Delta Y vs. Z');
    xlabel('Z location (mm)');
    ylabel('\Delta Y (mm)');

    subplot (2, 2, 3)
    scatter(num_compton{1,c+1}.z0, num_compton{1,c+1}.Del_z, '.');
    axis ([-slab_thickness/2 slab_thickness/2 -20 20]);
    title('\Delta Z vs. Z');
    xlabel('Z location (mm)');
    ylabel('\Delta Z (mm)');
    
    subplot (2, 2, 4)
    scatter(num_compton{1,c+1}.z0, num_compton{1,c+1}.Del_d, '.');
    axis ([-slab_thickness/2 slab_thickness/2 -20 20]);
    title('\Delta D vs. Z');
    xlabel('Z location (mm)');
    ylabel('\Delta D (mm)');
    
    % Paused to view plots (unpause to make faster)
    %pause;
    
    % Save plots as fig files.
    savefig(h,['SD_back_compton_' num2str(c)]);
    
    clf;
    
end

close all;

end