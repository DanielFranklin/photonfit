function plot_resnorm (inputTable)

    for c = 0:max(inputTable.compton);
        rows = inputTable.compton == c;
        vars = {'resnorm'};
        num_compton{c+1} = inputTable(rows,vars);

        %h(1) = 
        figure(1)
        % Histogram of total distance error of estimated gamma location, for
        % varying number of compton scatters.

        h1 = histogram(num_compton{1,c+1}.resnorm);
        title(['resnorm, compton scatters:' num2str(c)]);

        xlabel('resnorm');
        ylabel('Frequency');
        h.Normalization = 'probability';
        h.BinWidth = 10000;
        hold on
        
%         figure(2)
%         h2 = histogram(num_compton{1,c+1}.error);
%         title(['MSE, compton scatters:' num2str(c)]);
% 
%         xlabel('MSE');
%         ylabel('Frequency');
%         h.Normalization = 'probability';
%         h.BinWidth = 10;
%         hold on
        pause;

    end
    clf;
end