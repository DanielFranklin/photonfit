% Histograms for the number of photons detected on the back detector, front
% detector, the max (front or back) and total (front and back).

function photon_number(inputTable)

num_photons = inputTable.Num_phot;

figure(1)
h1 = histogram(num_photons)
h1.BinEdges = 0:100:5000;
title('Number of Photons per Event (Back)')
xlabel('Photons')
ylabel('Frequency')

end
