function plot_primary_secondaries (Hits)

all_primary_rows = find (Hits (:, 5) == 0);
optical_rows = find (Hits (:, 5) == 1);
P = (Hits (all_primary_rows, 8:10));
O = (Hits (optical_rows, 8:10));

figure(1);
plot3 (P(:, 1), P (:, 2), P (:, 3), '.r', O(:, 1), O (:, 2), O (:, 3), '.b');

max_rays = 20;

colour = ['g', 'b', 'c', 'm', 'y', 'k', 'w'];
ncolours = columns (colour);

figure(2);
clf;
hold on;

figure(3);
clf;
hold on;

current_event_id = Hits (all_primary_rows (1), 4);

x0 = 0;
y0 = 0;
z0 = 0;

% Iterate through all primaries that we can find (i.e. where parent ID = 0)
for np = 1 : max (size (all_primary_rows))
	prev_event_id = current_event_id;
	current_event_id = Hits (all_primary_rows (np), 4);
	
	if (current_event_id ~= prev_event_id)
		fprintf (stderr, 'New hit: press enter to continue\n');
		pause;

		figure (2);
		clf;
		hold on;
		
		figure (3);
		clf;
		hold on;

		x0 = 0;
		y0 = 0;
		z0 = 0;
	end
	
	px0 = x0;
	py0 = y0;
	pz0 = z0;

	x0 = Hits (all_primary_rows (np), 8);
	y0 = Hits (all_primary_rows (np), 9);
	z0 = Hits (all_primary_rows (np), 10);

% Get line of first optical event

	optical = all_primary_rows (np) + 1;
	rays = 0;
	
	figure(2);

	while (Hits (optical, 5) == 1) && (Hits (optical, 4) == current_event_id) && (rays < max_rays)
		plot3 ([x0; Hits(optical, 8)], [y0; Hits(optical,9)], [z0; Hits(optical,10)], sprintf ('%c-*', colour (1 + mod (np, ncolours))));
		optical++;
		rays++;
	end
	
	plot3 ([px0; x0], [py0; y0], [pz0; z0], 'r-+');
	axis ([100, 151, -100, 100, -100, 100]);

	figure (3);

	plot3 ([px0; x0], [py0; y0], [pz0; z0], 'r-+');
	axis ([100, 151, -100, 100, -100, 100]);
end

hold off;
