%% --- Script to concatenate and plot all "photonfit" output
% num_primaries = number of primaries for each GATE run.

function concatData_allDet(num_primaries)
% Directory to find all .dat files
% ds = datastore('/home/keenan/Documents/scint_slab_depth/photonfit/MATLAB_allDetConfig/*.dat', 'FileExtensions','.dat', 'Type', 'tabulartext');
ds = datastore([pwd '/*.dat'], 'FileExtensions','.dat', 'Type', 'tabulartext');

% Specifying parameters for the photonfit function.
det_dim_pix = [20 20];
det_dim_mm = [20 20];
atten_length = 100.0;
slab_thickness = 10.3;
fastmode = 1;
n1 = 1.82;
n2 = 1.52;
critical_angle_degrees = asind(n2/n1);

% Initial conditions for loop.
previousEvent = 0;
outputTable_back = table;
outputTable_front = table;
outputTable_both = table;

% Loop takes each file in the directory, creates an output table for each
% with photonfit, then concatenates them into a large singular output.
for i = 1:numel(ds.Files)

    filename = ds.Files{i,1};

    [output_back, output_front, output_both] = ...
        photonfit_allDetectors(filename, det_dim_pix, det_dim_mm, ...
        atten_length, critical_angle_degrees, slab_thickness, fastmode);
    
    % Change the event number to pick up from previous output.
    output_back.event = output_back.event + previousEvent;
    output_front.event = output_front.event + previousEvent;
    output_both.event = output_both.event + previousEvent;
    
    previousEvent = num_primaries * i; %max(output.event);
    
    % Concatenate new table with previous.
    outputTable_back = vertcat(outputTable_back, output_back);
    outputTable_front = vertcat(outputTable_front, output_front);
    outputTable_both = vertcat(outputTable_both, output_both);

end

% Save output table.
writetable(outputTable_back, 'finalOutput_backOnly.txt')
writetable(outputTable_front, 'finalOutput_frontOnly.txt')
writetable(outputTable_both, 'finalOutput_twosided.txt')

end
