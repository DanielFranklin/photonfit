function fh = gen_psa_args (atten_length, critical_angle)

fh = @(params, grid_coords) pointsourceattenuative_args (params, grid_coords, atten_length, critical_angle);
