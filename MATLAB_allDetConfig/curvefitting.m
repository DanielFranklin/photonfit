%% Error minimisation for curvefit.
% Script chooses an initial x,y position based off of the position of the
% maximum value in the data histogram. Initial position for z is randomized
% but not closer than 2.5 mm from the detector surface.

function [params, resnorm, exitflag] = curvefitting(row, col, yield, ...
    det_dim_mm, slab_thickness, X, Y, Z, objfunc, grid_coords, lb, ub, ...
    options)

[num_peaks] = size(row);

params = [];
resnorm = [];
residual = [];
exitflag = [];

for i = 1:5

    if num_peaks(1) > 1
        x_i = -(det_dim_mm(1)/2) + det_dim_mm(1).*rand(1);
        y_i = -(det_dim_mm(2)/2) + det_dim_mm(2).*rand(1);
    else
        x_i = X(row, col);
        y_i = Y(row, col); 
    end

    z_i = 2.5 + (slab_thickness - 5).*rand(1);

    initial_params = [x_i, y_i, z_i, 0, yield];

    [calc_params, calc_resnorm, calc_residual, calc_exitflag] = ...
        lsqcurvefit(objfunc, initial_params, grid_coords, Z, lb, ub, ...
        options);

    if isempty(params)
        params = calc_params;
        resnorm = calc_resnorm;
        residual = calc_residual;
        exitflag = calc_exitflag;

    elseif calc_resnorm < resnorm
        params = calc_params;
        resnorm = calc_resnorm;
        residual = calc_residual;
        exitflag = calc_exitflag;
    end
end
end