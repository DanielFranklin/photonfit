% Histograms for the number of photons detected on the back detector, front
% detector, the max (front or back) and total (front and back).

function photon_number_twosided(inputTable)

num_photons_back = inputTable.Num_phot_back;
num_photons_front = inputTable.Num_phot_front;
max_photons = max(num_photons_back.', num_photons_front.');
total_photons = num_photons_back + num_photons_front;

figure(1)
h1 = histogram(num_photons_back)
h1.BinEdges = 0:100:5000;
title('Number of Photons per Event (Back)')
xlabel('Photons')
ylabel('Frequency')

figure(2)
h2 = histogram(num_photons_front)
h2.BinEdges = 0:100:5000;
title('Number of Photons per Event (Front)')
xlabel('Photons')
ylabel('Frequency')

figure(3)
h3 = histogram(max_photons)
h3.BinEdges = 0:100:5000;
title('Number of Photons per Event (Max - Front or Back)')
xlabel('Photons')
ylabel('Frequency')

figure(4)
h4 = histogram(total_photons)
h4.BinEdges = 0:100:10000;
title('Number of Photons per Event (Total - Front and Back)')
xlabel('Photons')
ylabel('Frequency')

end
