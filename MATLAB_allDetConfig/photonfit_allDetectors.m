%% Annihilation photon position estimation.
% det_dim_pix and det_dim_mm are 2-element vectors of the detector
% dimensions in pixels and mm, respectively.
% 
% For the sim we are currently using, the slab is 2 cm square (i.e.  20
% mm).  It seems that about 10 pixels is reasonable. So call this as
% photonfit ('Hits.dat', [10 10], [20 20]);
%
% The axes of the subfigures are linked together; move one and you move the
% other too.
%
% fastmode skips drawing figures. Active/deactive as 1/0.
%
% Added in arguments for attenuation length and slab thickness of the
% scintillator (both in mm). Must include total thickness of epoxy 
% (normally 0.3mm).

function [output_back, output_front, output_both] = ...
    photonfit_allDetectors(filename, det_dim_pix, det_dim_mm, ...
    atten_length, critical_angle_degrees, slab_thickness, yield, fastmode)

% Load data file.
delimiterIn = ',';
headerlinesIn = 1;
dataStructure = importdata (filename, delimiterIn, headerlinesIn);

% Generate an XY grid representing the bounds of the detector.
step = det_dim_mm ./ det_dim_pix;

c{1} = (-(det_dim_pix(1)-1) / 2 : (det_dim_pix(1)-1) / 2) * step (1);
c{2} = (-(det_dim_pix(2)-1) / 2 : (det_dim_pix(2)-1) / 2) * step (2);

[X, Y] = meshgrid (c{1}, c{2});

grid_coords = zeros(det_dim_pix (2), det_dim_pix (1), 2);
grid_coords (:, :, 1) = X; 
grid_coords (:, :, 2) = Y;

% Filter out the optical photons from the main dataset, include only rows
% that specify their position of detection and the event ID. (Column 5 is
% parent ID and column 3 is pixel ID.)
PS_rows_back = dataStructure.data (:,8) == slab_thickness/2 | ...
    dataStructure.data (:,5) == 0;

PS_rows_front = dataStructure.data (:,8) == -slab_thickness/2 | ...
    dataStructure.data (:,5) == 0;

% Keep only event and parent ID's and X,Y,Z positions.
allData_back = dataStructure.data(PS_rows_back, [4:5 8:10]); 
allData_front = dataStructure.data(PS_rows_front, [4:5 8:10]);

% Split data up into photons hitting the front and the back detectors.
optical_rows_back = dataStructure.data(:, 5) == 1 ...
    & dataStructure.data(:, 8) == slab_thickness/2;
opticalHits_back = dataStructure.data(optical_rows_back, [4 8:11]);

optical_rows_front = dataStructure.data(:, 5) == 1 ...
    & dataStructure.data(:, 8) == -slab_thickness/2;
opticalHits_front = dataStructure.data(optical_rows_front, [4 8:11]);

% Find max optical hits (front or back)
max_optical_hits = max(max(opticalHits_back(:, 1)), ...
    max(opticalHits_front(:, 1)));

% Create a 3D matrix Z where each slice will be filled with the hit data
% from a single event.
Z_back = zeros(det_dim_pix(2), det_dim_pix(1), max_optical_hits + 1);
Z_front = Z_back;

Z_both = [Z_back Z_front];

% create an array that will be populated (later) with output data.
output_back = zeros(max(opticalHits_back(:, 1)) + 1, 18);
output_front = zeros(max(opticalHits_front(:, 1)) + 1, 18);
output_both = zeros(min(size(output_back, 1), size(output_front, 1)), 19);
step = det_dim_mm ./ det_dim_pix;

c{1} = (-(det_dim_pix(1)-1) / 2 : (det_dim_pix(1)-1) / 2) * step (1);
c{2} = (-(det_dim_pix(2)-1) / 2 : (det_dim_pix(2)-1) / 2) * step (2);

[X, Y] = meshgrid (c{1}, c{2});

grid_coords = zeros(det_dim_pix (2), det_dim_pix (1), 2);
grid_coords (:, :, 1) = X; 
grid_coords (:, :, 2) = Y;

%% --- Function Fitting ---
% Options an input parameters for lsqcurvefit
if fastmode == 1
    options = optimoptions('lsqcurvefit', 'Algorithm', ...
        'levenberg-marquardt', 'MaxIter', 1000, 'MaxFunEvals', 5000, ...
        'TolFun', 1e-20, 'TolX', 1e-20);
else
    options = optimoptions('lsqcurvefit', 'Algorithm', ...
        'levenberg-marquardt', 'Display', 'iter-detailed', 'MaxIter', ...
        1000, 'MaxFunEvals', 5000, 'TolFun', 1e-20, 'TolX', 1e-20);
end

lb = [];
ub = [];

objfunc_sing = gen_psa_args(atten_length, critical_angle_degrees);

objfunc_both = gen_psa_twosided(atten_length, critical_angle_degrees, ...
    slab_thickness);

for event = 1:max(opticalHits_back(:, 1)) + 1
    
	disp (event)
    
	% Filtering optical hits by event ID.
	optFilt_back = opticalHits_back(ismember(opticalHits_back(:, 1), ...
        event - 1), :); 
    optFilt_front = opticalHits_front(ismember(opticalHits_front(:, 1), ...
        event - 1), :); 
    
    if isempty(optFilt_back) || isempty(optFilt_front)
        continue          
    end
%     if isempty(optFilt_back) && isempty(optFilt_front)
%         continue          
%     end
	
	% Populating Z with optical hits on the XY plane. Each slice of Z is a
	% different event. Then plot. Bin centers need to be specified to
	% correctly centre histogram.

	Z_back(:, :, event) = ...
        hist3([optFilt_back(:, 4) optFilt_back(:, 3)], 'Ctrs', c)';
    
    Z_front(:, :, event) = ...
        hist3([optFilt_front(:, 4) optFilt_front(:, 3)], 'Ctrs', c)';
    
    Z_both(:, :, event) = [Z_back(:, :, event) Z_front(:, :, event)];
        
    if fastmode == 0
        % set all plot interpreters to latex - changes axes, legend and
        % tick labels.
        set(groot,'defaulttextinterpreter','latex');  
        set(groot, 'defaultAxesTickLabelInterpreter','latex');  
        set(groot, 'defaultLegendInterpreter','latex');
        
        % note row and column swapped here in hist3 to be consistant with
        % X and Y definitions. For export into Latex histogram2 may work
        % better.
        % Need to define bin edges for histogram2, since we can't use
        % centers.
        Xbinedges = (-(det_dim_pix(1)) / 2 : (det_dim_pix(1)) / 2) ...
            * step (1);
        Ybinedges = (-(det_dim_pix(2)) / 2 : (det_dim_pix(2)) / 2) ...
            * step (2);
        
        figure(1)
        histogram2(optFilt_front(:, 4), optFilt_front(:, 3), ...
            'XBinEdges', Xbinedges, 'YBinEdges', Ybinedges, ...
            'FaceColor', 'flat')
        title('Hits Front Detector')
        xlabel('x Position (mm)')
        ylabel('y Position (mm)')
        zlabel('Intensity (photons/mm$^{3}$)')
        axis tight
           
        figure(2)
        histogram2(optFilt_back(:, 4), optFilt_back(:, 3), 'XBinEdges', ...
            Xbinedges, 'YBinEdges', Ybinedges, 'FaceColor', 'flat')
        title('Hits Back Detector')
        xlabel('x Position (mm)')
        ylabel('y Position (mm)')
        zlabel('Intensity (photons/mm$^{3}$)')
        axis tight
    end
        
    % The initial parameters have been changed to randomized values to
    % avoid local minima. Positions have been confined to an interval
    % based on the physical size of the scintillator. For interval
    % (a,b), r_i = a + (a - b).*rand(N).
    % for loop calculates the parameters i times, then compares the resnorm 
    % of each and takes the values corresponding to the lowest.
    max_back = max(Z_back(:, :, event));
    [row, col] = find(ismember(Z_back(:, :, event), max_back));
    
    [params_back, resnorm_back, exitflag_back] = ...
        curvefitting(row, col, yield, det_dim_mm, slab_thickness, X, Y, ...
        Z_back(:, :, event), objfunc_sing, grid_coords, lb, ub, options);
    
    max_front = max(Z_front(:, :, event));
    [row, col] = find(ismember(Z_front(:, :, event), max_front));
    
    [params_front, resnorm_front, exitflag_front] = ...
        curvefitting(row, col, yield, det_dim_mm, slab_thickness, X, Y, ...
        Z_front(:, :, event), objfunc_sing, grid_coords, lb, ub, options);
    
    % Calculated parameters using both detectors for fit.
    if max_back > max_front
        [row, col] = find(ismember(Z_back, max_back));
    else
        [row, col] = find(ismember(Z_front, max_front));
    end
    
    [params_both, resnorm_both, exitflag_both] = ...
        curvefitting(row, col, yield, det_dim_mm, slab_thickness, X, Y, ...
        Z_both(:, :, event), objfunc_both, grid_coords, lb, ub, options);
	
	% Solution to the fitting function
    F_back = pointsourceattenuative_args(params_back, grid_coords, ...
        atten_length, critical_angle_degrees);
    
    F_front = pointsourceattenuative_args(params_front, grid_coords, ...
        atten_length, critical_angle_degrees);
    
	F_both = pointsourceattenuative_twosided(params_both, grid_coords, ...
        atten_length, critical_angle_degrees, slab_thickness);
    
    if fastmode == 0

        figure(3)
        mesh (X, Y, F_back);
        title('Fitting Function Back (Single)')
        xlabel('x Position (mm)')
        ylabel('y Position (mm)')
        zlabel('Intensity (photons/mm$^{3}$)')
        xlim([-(det_dim_pix(1)/2) (det_dim_pix(1)/2)])
        axis tight
            
        figure(4)
        mesh (X, Y, F_front);
        title('Fitting Function Front (Single)')
        xlabel('x Position (mm)')
        ylabel('y Position (mm)')
        zlabel('Intensity (photons/mm$^{3}$)')
        axis tight
        
        figure(5)
        mesh (X, Y, F_both(:, (det_dim_pix(1)+1) : (2*det_dim_pix(1))));
        title('Fitting Function Front (Double)')
        xlabel('x Position (mm)')
        ylabel('y Position (mm)')
        zlabel('Intensity (photons/mm$^{3}$)')
        xlim([-(det_dim_pix(1)/2) (det_dim_pix(1)/2)])
        axis tight
            
        figure(6)
        mesh (X, Y, F_both(:, 1:det_dim_pix(1)));
        title('Fitting Function Back (Double)')
        xlabel('x Position (mm)')
        ylabel('y Position (mm)')
        zlabel('Intensity (photons/mm$^{3}$)')
        axis tight
            
        sprintf ('Estimated x, y, z: %f %f %f', params_both (1), ...
            params_both (2), params_both (3));
        sprintf ('Est. yield: %f photons/primary', params_both (5));
    end
 
%% --- Plotting Primaries and Secondaries ---  
    
    % Filter the data by event #.
    allDataFilt_back = allData_back (ismember (allData_back(:, 1), ...
        event - 1), :);
    allDataFilt_front = allData_front (ismember (allData_front(:, 1), ...
        event - 1), :);
    
    if isempty(allDataFilt_back) || isempty(allDataFilt_front)
        continue
    end
    
    primary_rows_back = find(allDataFilt_back(:, 2) == 0);
    secondary_rows_back = find(allDataFilt_back(:, 2) == 1);
    
    primary_rows_front = find(allDataFilt_front(:, 2) == 0);
    secondary_rows_front = find(allDataFilt_front(:, 2) == 1);
        
    % Condition to filter out cases that have secondary data with no
    % primary data (cases which may interact with the epoxy orfastmode
    % meltmount. Also filter out cases with a low number of optical
    % photons.
    [num_optical_back] = size(secondary_rows_back);
    [num_optical_front] = size(secondary_rows_front);
    
    if isempty(primary_rows_back) && isempty(primary_rows_front)
        continue
%     elseif num_optical_back(1) < 100 || num_optical_front(1) < 100
%         continue
    end

    % Iterate through all primaries that we can find (i.e. where parent ID
    % is equal to 0).
    if fastmode == 0 
        plot_PS_allDet(primary_rows_back, allDataFilt_back, det_dim_mm, ...
            slab_thickness) % secondaries for back det.
        hold on
        
        plot_PS_allDet(primary_rows_front, allDataFilt_front, ...
            det_dim_mm, slab_thickness) % secondaries for front det.
        hold off
    end
    
    % Changing Z dimension to GATE coords (i.e. 0 at center of scint.)
    params_back(3) = (slab_thickness/2) - params_back(3);
    params_front(3) = params_front(3) - (slab_thickness/2);
    params_both(3) = (slab_thickness/2) - params_both(3);  
        
    % Fill output matrix with event #, calculated values from
    % lscurvefit, # of compton scatters, mean squared error and
    % exitflag from lsqcurvefit (to make sure an answer is reached).
    output_back(event, 1) = event;
    output_front(event, 1) = event;
    output_both(event, 1) = event;
    
    output_back(event, 2:6) = params_back(1:5);
    output_front(event, 2:6) = params_front(1:5);
    output_both(event, 2:6) = params_both(1:5);
    
    output_back(event, 7) = optFilt_back(end, 5);
    output_front(event, 7) = optFilt_front(end, 5);
    output_both(event, 7) = optFilt_back(end, 5); % Doen't matter if we use 
    % front or back data.
    
    output_back(event, 8) = immse(Z_back(:, :, event), F_back);
    output_front(event, 8) = immse(Z_front(:, :, event), F_front);
    output_both(event, 8) = immse(Z_both(:, :, event), F_both);
    
    output_back(event, 9) = resnorm_back;
    output_front(event, 9) = resnorm_front;
    output_both(event, 9) = resnorm_both;
    
    output_back(event, 10) = exitflag_back;
    output_front(event, 10) = exitflag_front;
    output_both(event, 10) = exitflag_both;

    % Adding x,y,z position of the first point of interaction of
    % primary with scintillator (x0, y0, z0).  
    output_back(event, 11:13) = ...
        [(allDataFilt_back(primary_rows_back(1), 3)), ...
        (allDataFilt_back(primary_rows_back(1), 4)), ...
        (allDataFilt_back(primary_rows_back(1), 5))];
    
    output_front(event, 11:13) = output_back(event, 11:13);
    
    output_both(event, 11:13) = output_back(event, 11:13);
       
    %Then add error between calculated values and actual position
    %(del_x, del_y, del_z). Abs. values removed.
    output_back(event, 14:16) = ...
        [(output_back(event, 2) - output_back(event, 13)), ...
        (output_back(event, 3) - output_back(event, 12)), ...
        (output_back(event, 4) - output_back(event, 11))];
    
    output_front(event, 14:16) = ...
        [(output_front(event, 2) - output_front(event, 13)), ...
        (output_front(event, 3) - output_front(event, 12)), ...
        (output_front(event, 4) - output_front(event, 11))];
    
    output_both(event, 14:16) = ...
        [(output_both(event, 2) - output_both(event, 13)), ...
        (output_both(event, 3) - output_both(event, 12)), ...
        (output_both(event, 4) - output_both(event, 11))];
        
    % and total error (del_d).
    output_back(event, 17) = sqrt(output_back(event, 14).^2 + ...
        output_back(event, 15).^2 + output_back(event,16).^2);
    
    output_front(event, 17) = sqrt(output_front(event, 14).^2 + ...
        output_front(event, 15).^2 + output_front(event,16).^2);
    
    output_both(event, 17) = sqrt(output_both(event, 14).^2 + ...
        output_both(event, 15).^2 + output_both(event,16).^2);
    
    % To count the total number of photons on each detector surface.
    output_back(event, 18) = num_optical_back(1);
    
    output_front(event, 18) = num_optical_front(1);
    
    output_both(event, 18) = num_optical_back(1);
    output_both(event, 19) = num_optical_front(1);
            

    if fastmode == 0
        pause;
         % clear all figures.
        clf(1); clf(2); clf(3); clf(4); clf(5); clf(6); clf(7); clf(8);
    end 
    
        
end

 close all;

%% --- Output result in a table ---

% Convert output into a table for data cleaning.
output_back = array2table(output_back, 'VariableNames', {'event', 'x', ...
    'y', 'z', 'attenuation', 'lightYield', 'compton', 'error', ...
    'resnorm', 'exitflag', 'z0', 'y0', 'x0', 'Del_x', 'Del_y', 'Del_z', ...
    'Del_d', 'Num_phot_back'});

output_front = array2table(output_front, 'VariableNames', {'event', ...
    'x', 'y', 'z', 'attenuation', 'lightYield', 'compton', 'error', ...
    'resnorm', 'exitflag', 'z0', 'y0', 'x0', 'Del_x', 'Del_y', 'Del_z', ...
    'Del_d', 'Num_phot_front'});

output_both = array2table(output_both, 'VariableNames', {'event', 'x', ...
    'y', 'z', 'attenuation', 'lightYield', 'compton', 'error', ...
    'resnorm', 'exitflag', 'z0', 'y0', 'x0', 'Del_x', 'Del_y', 'Del_z', ...
    'Del_d', 'Num_phot_back', 'Num_phot_front'});

% to cut out zero entries in the data set
output_back(output_back.event == 0,:) = [];
output_front(output_front.event == 0,:) = [];
output_both(output_both.event == 0,:) = [];
end
