% Parameters are x, y, z of point source, linear attenuation coefficient of
% medium, and original photon yield (source brightness).  Question: can we
% fix the latter and assume it is constant?  This may help distinguishing
% between scattered and pure photoelectric events.

function F = pointsourceattenuative_args (params, gridcoords, atten_length, critical_angle)

atten_co = 1/atten_length; % attenutation coefficient = 1/attenuation length (in mm)

dx = gridcoords (1, 2, 1) - gridcoords (1, 1, 1); % size of pixel (mm) in x dimension.
dy = gridcoords (2, 1, 2) - gridcoords (1, 1, 2); % size of pixel (mm) in y dimension.

dsq = (gridcoords (:, :, 1) - params(1)) .^ 2 + (gridcoords (:, :, 2) - params(2)) .^ 2 + params(3) ^ 2;

theta = acosd (params (3) ./ (sqrt (dsq)));

% In units of photons/pixel rather than photons/unit area
F = params(5) * params(3) .* exp (-sqrt (dsq) * atten_co) ./ (4 * pi * dsq .^ 1.5) * dx * dy .* (theta < critical_angle);

%imagesc (F);

%pause;
