%% Detector Logic
% Scrip to compare the position estimation of the gamma interaction, 
% between front only, back only and two detector systems. 

function detectorLogic(inputTable_back, inputTable_front, inc_angle)

% set plots to Latex interpreter.
set(groot,'defaulttextinterpreter','latex');  
set(groot, 'defaultAxesTickLabelInterpreter','latex');  
set(groot, 'defaultLegendInterpreter','latex');

% Pre-allocate output array and pre-fill event, compton and particle
% interaction point.
output = zeros(size(inputTable_back, 1), 6);
output(:, 1) = inputTable_back.event;
output(:, 6) = inputTable_back.compton;

% Take the difference in X,Y,Z estimations for front and back detectors,
% along with the total distance (DelD). Fill in the output table.
for n = 1:size(inputTable_back, 1)
    DelX = abs(inputTable_back.x(n) - inputTable_front.x(n));
    DelY = abs(inputTable_back.y(n) - inputTable_front.y(n));
    DelZ = abs(inputTable_back.z(n) - inputTable_front.z(n));
    DelD = sqrt(DelX^2 + DelY^2 + DelZ^2);
    
    output(n, 2:5) = [DelX DelY DelZ DelD];
end
% Dimensions cell will be used for naming plots.
dimensions = {'X' 'Y' 'Z' 'D'};

for d = 2:5 % Run through each dimension DelX,Y,Z and total dist. (DelD).
    plotData = zeros(4, 4);
    % Split up the output data, depending on the number of compton
    % interactions.
    for c = 0:1:max(inputTable_back.compton)
        if c <= 2
            compFilt = output(output(:, 6) == c, :);
            med = median(compFilt(:, d));
            Q1 = prctile(compFilt(:, d), 25);
            Q3 = prctile(compFilt(:, d), 75);

            plotData(c+1, 1) = c;
            plotData(c+1, 2) = med;
            plotData(c+1, 3) = Q1;
            plotData(c+1, 4) = Q3;
        else
            compFilt = output(output(:, 6) >= 3, :);
            med = median(compFilt(:, d));
            Q1 = prctile(compFilt(:, d), 25);
            Q3 = prctile(compFilt(:, d), 75);

            plotData(c+1, 1) = c;
            plotData(c+1, 2) = med;
            plotData(c+1, 3) = Q1;
            plotData(c+1, 4) = Q3;
            break
        end
    end

    % To fit a polynomial (3rd order) to the data points, use polyfit and
    % polyval. For a smooth curve, x values to be fed in the functions have 
    % been defined below.
    med_fit = polyfit(plotData(:, 1), plotData(:, 2), 3);
    Q1_fit = polyfit(plotData(:, 1), plotData(:, 3), 3);
    Q3_fit = polyfit(plotData(:, 1), plotData(:, 4), 3);

    x = 0:0.01:3;

    med_fun = polyval(med_fit, x);
    Q1_fun = polyval(Q1_fit, x);
    Q3_fun = polyval(Q3_fit, x);

    % Plot calculated data points and corresponding fitting functions.
    h = figure(1);
    hold on
    plot(plotData(:, 1), plotData(:, 2), '.')
    plot(plotData(:, 1), plotData(:, 3), 'x')
    plot(plotData(:, 1), plotData(:, 4), '+')
    p1 = plot(x, med_fun, '-');
    p2 = plot(x, Q1_fun, '-');
    p3 = plot(x, Q3_fun, '-');
    hold off
    ylim([0 round(Q3 / 5)*5 + 5])
    xlabel('Compton Scatters')
    ylabel('Position Estimation Difference (mm)')
    title(['Position Estimation Discrepancy - $\Delta$', dimensions{d-1}])
    legend([p1 p2 p3], 'median', 'Q1', 'Q3', 'Location', 'southoutside', ...
        'Orientation', 'horizontal')
    grid on
    
    savefig(h, ['FB_discrepancy_', num2str(inc_angle), 'deg_', ...
        dimensions{d-1}])
    saveas(h, ['FB_discrepancy_', num2str(inc_angle), 'deg_', ...
        dimensions{d-1}], 'epsc')
    %pause
    clf;
end
close all
end