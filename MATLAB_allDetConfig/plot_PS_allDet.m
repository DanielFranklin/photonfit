
function plot_PS (primary_rows, allDataFilt, det_dim_mm, slab_thickness)

    x0 = -slab_thickness/2;
    y0 = 0;
    z0 = 0;
    
    colour = ['g', 'b', 'c', 'm', 'y', 'k', 'w'];
    ncolours = length (colour);

for np = 1 : max (size (primary_rows))

    px0 = x0;
    py0 = y0;
    pz0 = z0;

    x0 = allDataFilt (primary_rows (np), 3); % posX
    y0 = allDataFilt (primary_rows (np), 4); % posY
    z0 = allDataFilt (primary_rows (np), 5); % posZ
    % Get line of first optical event
    optical = primary_rows (np) + 1;
    rays = 1;
    
    % Need to calculate max rays for each compton scatter. Calculates
    % number of rows between primary rows or number of rows after the final
    % primary. Fixes indexing issues.
    if np+1 <= max (size (primary_rows))
        max_rays = primary_rows(np+1) - primary_rows(np) - 1;
        max_rays = min(20, max_rays); % max limit 20.
    else
        max_rays = size(allDataFilt,1) - primary_rows(np);
        max_rays = min(20, max_rays); % max limit 20.
    end
    
    % only run if optical row is within the matrix size (to prevent
    % indexing errors for primaries without secondary photons.
    if optical <= size(allDataFilt,1)

        figure(7);
%         ax3 = subplot (3, 2, 5);
        hold on

        % Column 5 is parent ID (0 for primary gamma rays and 1 for optical
        % photons). So while parent ID = 1 (optical) and event ID = current_event_id
        % (i.e. the same event) and rays are < 20 (to speed up processing),
        % then plot X,Y,Z of each hit, then increment to next optical photon
        % (up to 19 rays).
        while (allDataFilt (optical, 2) == 1) && (rays < max_rays)
            plot3 ([x0; allDataFilt(optical, 3)], [y0; allDataFilt(optical,4)], ...
                [z0; allDataFilt(optical,5)], sprintf ('%c-*', colour (1 + mod (np, ncolours))));
            optical = optical + 1;
            rays = rays + 1;
        end        
        plot3 ([px0; x0], [py0; y0], [pz0; z0], 'r-+');
        axis equal;
        axis ([-slab_thickness/2, slab_thickness/2, -det_dim_mm(2)/2, det_dim_mm(2)/2, -det_dim_mm(1)/2, det_dim_mm(1)]);

        xlabel('z Position (mm)')
        ylabel('y Position (mm)')
        hold off

        figure (8);
%         ax4 = subplot (3, 2, 6);
        plot3 ([px0; x0], [py0; y0], [pz0; z0], 'r-+');
        axis equal;
        axis ([-slab_thickness/2, slab_thickness/2, -det_dim_mm(2)/2, det_dim_mm(2)/2, -det_dim_mm(1)/2, det_dim_mm(1)]);
        view(0,90)
        xlabel('z Position (mm)')
        ylabel('y Position (mm)')

    end

end
end