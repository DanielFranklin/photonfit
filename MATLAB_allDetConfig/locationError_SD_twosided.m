
% Function to return a histogram of primary particle first hit location
% error, as calculated by the photonfit function. Then a scatter plot of
% error in the x,y,z directions with respect to the primary's distance from
% the detector.

function locationError_SD_twosided(inputTable, slab_thickness)
        
% Set plots to Latex interpreter.
set(groot,'defaulttextinterpreter','latex');  
set(groot, 'defaultAxesTickLabelInterpreter','latex');  
set(groot, 'defaultLegendInterpreter','latex');

% filter out estimates that are not inside the scintillator volume.
filter = inputTable.x >= -10 & inputTable.x <= 10  ...
    & inputTable.y >= -10 & inputTable.y <= 10 ...
    & inputTable.z >= -(slab_thickness/2) & inputTable.z <= (slab_thickness/2);

inputTable = inputTable(filter,:);

% arrange into sets depending on # of compton scatters.
for c = 0:max(inputTable.compton)
    rows = inputTable.compton == c;
    vars = {'z0','Del_x','Del_y','Del_z','Del_d'};
    num_compton{c+1} = inputTable(rows,vars);
    
    figure(1)
    % Histogram of total distance error of estimated gamma location, for
    % varying number of compton scatters.
    edges = 0:0.5:20;
    histogram(num_compton{1,c+1}.Del_d, edges);
%     title(['\Delta D Histogram, compton scatters:' num2str(c)]);
    xlim([0 20]);
    xlabel('$\Delta$ D (mm)');
    ylabel('Frequency');

%     h(2) = figure(2)
    % Scatter plots for error in x,y,z position and total distance (d) vs.
    % distance from detector (Z), for varying number of comton scatters.
%     subplot (2, 2, 1)
    figure(2)
    scatter(num_compton{1,c+1}.z0, num_compton{1,c+1}.Del_x, '.');
    axis ([-slab_thickness/2 slab_thickness/2 -10 10]);
%     title('$\Delta$ X vs. Z');
    xlabel('Z Position (mm)');
    ylabel('X Error (mm)');
    grid on

%     subplot (2, 2, 2)
    figure(3)
    scatter(num_compton{1,c+1}.z0, num_compton{1,c+1}.Del_y, '.');
    axis ([-slab_thickness/2 slab_thickness/2 -10 10]);
%     title('$\Delta$ Y vs. Z');
    xlabel('Z Position (mm)');
    ylabel('Y Error (mm)');
    grid on

%     subplot (2, 2, 3)
    figure(4)
    scatter(num_compton{1,c+1}.z0, num_compton{1,c+1}.Del_z, '.');
    axis ([-slab_thickness/2 slab_thickness/2 -20 20]);
%     title('$\Delta$ Z vs. Z');
    xlabel('Z Position (mm)');
    ylabel('Z Error (mm)');
    grid on
    
%     subplot (2, 2, 4)
    figure(5)
    scatter(num_compton{1,c+1}.z0, num_compton{1,c+1}.Del_d, '.');
    axis ([-slab_thickness/2 slab_thickness/2 0 20]);
%     title('$\Delta$ D vs. Z');
    xlabel('Z Position (mm)');
    ylabel('Total Error (mm)');
    grid on
    
    % Paused to view plots (unpause to make faster)
    pause;
    
    % Save plots as fig files.
%     savefig(h,['SD_twosided_compton_' num2str(c)]);
    
    clf;
    
end

close all;

end