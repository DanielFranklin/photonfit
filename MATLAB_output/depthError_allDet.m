%% Find X,Y,Z error for variable Z (scintillator depth)
% Front, back and both detector error will be plotted together.

function h = depthError_allDet(inputTable_back, inputTable_front, ...
    inputTable_both, thickness)

C = {inputTable_back, inputTable_front, inputTable_both};

numBins = round(thickness); % Rounded to nearest inter.
binWidth = thickness/numBins;

errStats = zeros(numBins, 19);

for i = 1:numBins  
    % Filtering data for values within the bin only.
    bin = inputTable_back.z0 >= -(thickness/2) + (i-1)*binWidth ...
        & inputTable_back.z0 < -(thickness/2) + i*binWidth;
    
    % Filling out errStats matrix. First column is the bin number (from 
    % the front of the detector to the back). Columns 2-7 are error stats
    % for back detector only C{1}, 8-13 are front only and 14-19 are both.
    % Error stats are X,Y,Z mean and standard dev for each bin.
    errStats(i, 1) = i;
   
    for j = 1:3
        X_meanError = mean(C{j}.Del_x(bin));
        Y_meanError = mean(C{j}.Del_y(bin));
        Z_meanError = mean(C{j}.Del_z(bin));
        X_stdError = std(C{j}.Del_x(bin));
        Y_stdError = std(C{j}.Del_y(bin));
        Z_stdError = std(C{j}.Del_z(bin));

        errStats(i, 2 + (j-1)*6) = X_meanError;
        errStats(i, 3 + (j-1)*6) = X_stdError;
        errStats(i, 4 + (j-1)*6) = Y_meanError;
        errStats(i, 5 + (j-1)*6) = Y_stdError;
        errStats(i, 6 + (j-1)*6) = Z_meanError;
        errStats(i, 7 + (j-1)*6) = Z_stdError;
    end   
end

% Remore rows in errStats which don't have any data.
errStats(any(isnan(errStats), 2), :) = [];
errStats(any(errStats==0, 2), :) = [];

% Cell D contains titles and axes titles, for the plots generated with the
% following loop.
D = {'Error X (mm)' 'Error X dimension' 'Error Y (mm)' ...
    'Error Y dimension' 'Error Z (mm)' 'Error Z dimension'};

for n = 1:3
    % Fit a third order polynomial to each error set.
    fit_mean_both = polyfit(errStats(:, 1), errStats(:, 2 + (n-1)*2), 3);
    fit_std1_both = polyfit(errStats(:, 1), errStats(:, 2 + (n-1)*2) ...
        + errStats(:, 3 + (n-1)*2), 3);
    fit_std2_both = polyfit(errStats(:, 1), errStats(:, 2 + (n-1)*2) ...
        - errStats(:, 3 + (n-1)*2), 3);
    fit_mean_back = polyfit(errStats(:, 1), errStats(:, 8 + (n-1)*2), 3);
    fit_std1_back = polyfit(errStats(:, 1), errStats(:, 8 + (n-1)*2) ...
        + errStats(:, 9 + (n-1)*2), 3);
    fit_std2_back = polyfit(errStats(:, 1), errStats(:, 8 + (n-1)*2) ...
        - errStats(:, 9 + (n-1)*2), 3);
    fit_mean_front = polyfit(errStats(:, 1), errStats(:, 14 + (n-1)*2), 3);
    fit_std1_front = polyfit(errStats(:, 1), errStats(:, 14 + (n-1)*2) ...
        + errStats(:, 15 + (n-1)*2), 3);
    fit_std2_front = polyfit(errStats(:, 1), errStats(:, 14 + (n-1)*2) ...
        - errStats(:, 15 + (n-1)*2), 3);

    fun1_both = polyval(fit_mean_both, errStats(:, 1));
    fun2_both = polyval(fit_std1_both, errStats(:, 1));
    fun3_both = polyval(fit_std2_both, errStats(:, 1));
    fun1_back = polyval(fit_mean_back, errStats(:, 1));
    fun2_back = polyval(fit_std1_back, errStats(:, 1));
    fun3_back = polyval(fit_std2_back, errStats(:, 1));
    fun1_front = polyval(fit_mean_front, errStats(:, 1));
    fun2_front = polyval(fit_std1_front, errStats(:, 1));
    fun3_front = polyval(fit_std2_front, errStats(:, 1));
    
    h(n) = figure(n)
    hold on
    % Plot the X,Y,Z mean and std error.
    plot(errStats(:, 1), errStats(:, 2 + (n-1)*2), 'r.');
    plot(errStats(:, 1), errStats(:, 2 + (n-1)*2) + ...
        errStats(:, 3 + (n-1)*2), 'rx');
    plot(errStats(:, 1), errStats(:, 2 + (n-1)*2) - ...
        errStats(:, 3 + (n-1)*2), 'rx')
    plot(errStats(:, 1), errStats(:, 8 + (n-1)*2), 'b.');
    plot(errStats(:, 1), errStats(:, 8 + (n-1)*2) + ...
        errStats(:, 9 + (n-1)*2), 'bx')
    plot(errStats(:, 1), errStats(:, 8 + (n-1)*2) - ...
        errStats(:, 9 + (n-1)*2), 'bx')
    plot(errStats(:, 1), errStats(:, 14 + (n-1)*2), 'g.');
    plot(errStats(:, 1), errStats(:, 14 + (n-1)*2) + ...
        errStats(:, 15 + (n-1)*2), 'gx')
    plot(errStats(:, 1), errStats(:, 14 + (n-1)*2) - ...
        errStats(:, 15 + (n-1)*2), 'gx')
    
    % Plot the polynomial previously calculated.
    p1 = plot(errStats(:, 1), fun1_both, 'm-')
    plot(errStats(:, 1), fun2_both, 'm-')
    plot(errStats(:, 1), fun3_both, 'm-')
    p2 = plot(errStats(:, 1), fun1_back, 'c-')
    plot(errStats(:, 1), fun2_back, 'c-')
    plot(errStats(:, 1), fun3_back, 'c-')
    p3 = plot(errStats(:, 1), fun1_front, 'y-')
    plot(errStats(:, 1), fun2_front, 'y-')
    plot(errStats(:, 1), fun3_front, 'y-')

    xlabel('bin number')
    ylabel(D{1 + (n-1)*2})
    title(D{2 + (n-1)*2})
    legend([p1 p2 p3], 'both', 'back', 'front')
    grid on
    hold off
end
end

