%% Detector stats for repeat of each category
% ie for C1 C2 C3 PE
% --- Median and mean error in X, Y and Z, IQR and Standard deviation -----
function outputStats = categoryStats(inputTable)

med_x = median(inputTable.Del_x);
med_y = median(inputTable.Del_y);
med_z = median(inputTable.Del_z);
med_all = median(inputTable.Del_d);

mean_x = mean(inputTable.Del_x);
mean_y = mean(inputTable.Del_y);
mean_z = mean(inputTable.Del_z);
mean_all = mean(inputTable.Del_d);

IQR_x = iqr(inputTable.Del_x);
IQR_y = iqr(inputTable.Del_y);
IQR_z = iqr(inputTable.Del_z);
IQR_all = iqr(inputTable.Del_d);

SD_x = std(inputTable.Del_x);
SD_y = std(inputTable.Del_y);
SD_z = std(inputTable.Del_z);
SD_all = std(inputTable.Del_d);

% Error = {'X Error (mm)';'Y Error (mm)';'Z Error (mm)';'Total Error (mm)'};
% Median = [med_x;med_y;med_z;med_all];
% Mean = [mean_x;mean_y;mean_z;mean_all];
% IQR = [IQR_x;IQR_y;IQR_z;IQR_all];
% SD = [SD_x;SD_y;SD_z;SD_all];
% 
% outputStats = table(Median, Mean, IQR, SD, 'RowNames', Error);

X = [med_x mean_x IQR_x SD_x];
Y = [med_y mean_y IQR_y SD_y];
Z = [med_z mean_z IQR_z SD_z];
D = [med_all mean_all IQR_all SD_all];

tab = [X Y Z D];
outputStats = array2table(tab);
outputStats.Properties.VariableNames = {'Median_X' 'Mean_X' 'IQR_X' 'SD_X' ...
    'Median_Y' 'Mean_Y' 'IQR_Y' 'SD_Y' 'Median_Z' 'Mean_Z' 'IQR_Z' 'SD_Z' ...
    'Median_D' 'Mean_D' 'IQR_D' 'SD_D'};

end