%% X,Y,Z error for variable Z (scintillator depth)
% Import .txt file manually as a table.

function h = depthError(inputTable, thickness)

numBins = round(thickness); % Rounded to nearest inter.
binWidth = thickness/numBins;

errStats = zeros(numBins, 7); % To be filled with error stat data.

for n = 1:numBins
    
    % Filtering data for values within the bin only.
    bin = inputTable.z0 >= -(thickness/2) + (n-1)*binWidth ...
        & inputTable.z0 < -(thickness/2) + n*binWidth;
    
    X_meanError = mean(inputTable.Del_x(bin));
    X_stdError = std(inputTable.Del_x(bin));
    
    Y_meanError = mean(inputTable.Del_y(bin));
    Y_stdError = std(inputTable.Del_y(bin));
    
    Z_meanError = mean(inputTable.Del_z(bin));
    Z_stdError = std(inputTable.Del_z(bin));
    
    errStats(n, 1) = n;
    errStats(n, 2) = X_meanError;
    errStats(n, 3) = X_stdError;
    errStats(n, 4) = Y_meanError;
    errStats(n, 5) = Y_stdError;
    errStats(n, 6) = Z_meanError;
    errStats(n, 7) = Z_stdError;
   
end

h(1) = figure(1)
p1 = plot(errStats(:, 1), errStats(:, 2), 'm.');
hold on
p2 = plot(errStats(:, 1), errStats(:, 2) + errStats(:, 3), 'gx');
plot(errStats(:, 1), errStats(:, 2) - errStats(:, 3), 'gx')
xlabel('bin number')
ylabel('Error X (mm)')
title('Error X dimension')
legend([p1 p2], 'mean', 'std')
hold off
    
h(2) = figure(2)
p1 = plot(errStats(:, 1), errStats(:, 4), 'm.');
hold on
p2 = plot(errStats(:, 1), errStats(:, 4) + errStats(:, 5), 'gx');
plot(errStats(:, 1), errStats(:, 4) - errStats(:, 5), 'gx')
xlabel('bin number')
ylabel('Error Y (mm)')
title('Error Y dimension')
legend([p1 p2], 'mean', 'std')
hold off

h(3) = figure(3)
p1 = plot(errStats(:, 1), errStats(:, 6), 'm.');
hold on
p2 = plot(errStats(:, 1), errStats(:, 6) + errStats(:, 7), 'gx');
plot(errStats(:, 1), errStats(:, 6) - errStats(:, 7), 'gx')
xlabel('bin number')
ylabel('Error Z (mm)')
title('Error Z dimension')
legend([p1 p2], 'mean', 'std')
hold off

end