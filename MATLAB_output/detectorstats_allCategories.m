%% Script to analyse detector output data
% 
% slab_thickness is *without* the 0.3mm from epoxy and meltmount.
function [outputComp, outputInter, outputStats] = detectorstats_allCategories(inputTable, ...
    num_events, slab_thickness)

% Removing estimations which are outside the scintillator volume.
filter = inputTable.x >= -10 & inputTable.x <= 10 & ...
inputTable.y >= -10 & inputTable.y <= 10 & ...
inputTable.z >= -slab_thickness/2 & inputTable.z <= slab_thickness/2;
inputTable = inputTable(filter, :);

% Percentage of Hits registered as photoelectric / compton
photoE_idx = inputTable.compton == 0;
comp_1_idx = inputTable.compton == 1;
comp_2_idx = inputTable.compton == 2;
comp_3_idx = inputTable.compton == 3;
comp_4_idx = inputTable.compton == 4;
comp_5om_idx = inputTable.compton >= 5;

percent_inter = sum(inputTable.event ~= 0) / num_events * 100;
percent_photoE = sum(photoE_idx) / sum(inputTable.compton ~= -1) * 100;
percent_comp_1 = sum(comp_1_idx) / sum(inputTable.compton ~= -1) * 100;
percent_comp_2 = sum(comp_2_idx) / sum(inputTable.compton ~= -1) * 100;
percent_comp_3 = sum(comp_3_idx) / sum(inputTable.compton ~= -1) * 100;
percent_comp_4 = sum(comp_4_idx) / sum(inputTable.compton ~= -1) * 100;
percent_comp_5OM = sum(comp_5om_idx) / sum(inputTable.compton ~= -1) * 100;

scatter = {'PhotoE', 'Comp1', 'Comp2', 'Comp3', 'Comp4', ...
    'Comp5om', 'Interaction'};
percent = [percent_photoE percent_comp_1 percent_comp_2 ...
    percent_comp_3 percent_comp_4 percent_comp_5OM percent_inter];

outputComp = array2table(percent, 'VariableNames', scatter, 'rowNames', ...
    {'percent'});

% Try this with the scatterCount change, if it still doesn't work filter also using energy--------------------------------------------------------

% Percentage Hits by number of interactions in the scintillator.
% Distinguish between comptons which escape and those that are PE absorbed.
% Energy is slightly less for rounding errors.
photoE_idx = inputTable.compton == 0; %& inputTable.energyDeposited >= 0.51099;
comp_1_esc_idx = inputTable.compton == 1 & inputTable.energyDeposited < 0.51099; %& inputTable.interactions == 1; 
comp_1_PE_idx = inputTable.compton == 1 & inputTable.energyDeposited >= 0.51099; %& inputTable.interactions >= 2 
comp_2_esc_idx = inputTable.compton == 2 & inputTable.energyDeposited < 0.51099; %& inputTable.interactions == 2;
comp_2_PE_idx = inputTable.compton == 2 & inputTable.energyDeposited >= 0.51099; %& inputTable.interactions >= 3;
comp_3_esc_idx = inputTable.compton == 3 & inputTable.energyDeposited < 0.51099; %& inputTable.interactions == 3;
inter_4om_idx = inputTable.compton == 3 & inputTable.energyDeposited >= 0.51099 | inputTable.compton >= 4; %& inputTable.interactions >= 4;

percent_inter = sum(inputTable.event ~= 0) / num_events * 100;
percent_photoE = sum(photoE_idx) / sum(inputTable.compton ~= -1) * 100;
percent_comp_1_esc = sum(comp_1_esc_idx) / sum(inputTable.compton ~= -1) * 100;
percent_comp_1_PE = sum(comp_1_PE_idx) / sum(inputTable.compton ~= -1) * 100;
percent_comp_2_esc = sum(comp_2_esc_idx) / sum(inputTable.compton ~= -1) * 100;
percent_comp_2_PE = sum(comp_2_PE_idx) / sum(inputTable.compton ~= -1) * 100;
percent_comp_3_esc = sum(comp_3_esc_idx) / sum(inputTable.compton ~= -1) * 100;
percent_inter_4om = sum(inter_4om_idx) / sum(inputTable.compton ~= -1) * 100;

scatter = {'PhotoE', 'Comp1_Esc', 'Comp1_PE', ...
    'Comp2_Esc', 'Comp2_PE', 'Comp3_Esc', 'Inter4om', 'Interaction_total'};
percent = [percent_photoE percent_comp_1_esc percent_comp_1_PE ...
    percent_comp_2_esc percent_comp_2_PE percent_comp_3_esc ...
    percent_inter_4om percent_inter];

outputInter = array2table(percent, 'VariableNames', scatter, 'rowNames', ...
    {'percent'});

% sum(outputInter{:, 2:end})

% Filtering again to find error stats for either a particular number of 
% compton scatters, or a particular number of interactions (compton and PE)
PE_table = inputTable(photoE_idx, :);
PE_stats = categoryStats(PE_table);

C1_table = inputTable(comp_1_idx, :);
C1_stats = categoryStats(C1_table);

C1_esc_table = inputTable(comp_1_esc_idx, :);
C1_esc_stats = categoryStats(C1_esc_table);

C1_PE_table = inputTable(comp_1_PE_idx, :);
C1_PE_stats = categoryStats(C1_PE_table);

C2_table = inputTable(comp_2_idx, :);
C2_stats = categoryStats(C2_table);

C2_esc_table = inputTable(comp_2_esc_idx, :);
C2_esc_stats = categoryStats(C2_esc_table);

C2_PE_table = inputTable(comp_2_PE_idx, :);
C2_PE_stats = categoryStats(C2_PE_table);

comp_3om_idx = inputTable.compton >= 3;

C3om_table = inputTable(comp_3om_idx, :);
C3om_stats = categoryStats(C3om_table);

C3_esc_table = inputTable(comp_3_esc_idx, :);
C3_esc_stats = categoryStats(C3_esc_table);

Inter_4om_table = inputTable(inter_4om_idx, :);
Inter_4om_stats = categoryStats(Inter_4om_table);

outputStats = [PE_stats; C1_stats; C1_esc_stats; C1_PE_stats; C2_stats; ...
    C2_esc_stats; C2_PE_stats; C3om_stats; C3_esc_stats; Inter_4om_stats]

outputStats.Properties.RowNames = {'PE', 'C1_all', 'C1_esc', 'C1_PE', ...
    'C2_all', 'C2_esc', 'C2_PE', 'C3+', 'C3_esc', '4+_inter'};

end
