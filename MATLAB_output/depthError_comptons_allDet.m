% Script to seperate a photonfit output table into groups depending on the
% number of compton scatters, then apply the depthError function to each
% (i.e. X,Y,Z error with variable Z). Also an overall depthError will be 
% done with all data.

% Plots front detector only, back detector only and both on the same plot.

function depthError_comptons_allDet(inputTable_back, inputTable_front, ...
    inputTable_both, thickness, num_events)

% set plots to Latex interpreter.
set(groot,'defaulttextinterpreter','latex');  
set(groot, 'defaultAxesTickLabelInterpreter','latex');  
set(groot, 'defaultLegendInterpreter','latex');

% Split to input table into sub-tables, depending on the number of compton
% scatters. Save output plots of X,Y,Z error, binned with varying Z. (1mm
% bins). Photoelectrics and compton scatter (1-4) are plotted individually.
% Due to their lower number, comptons 5+ are grouped together. 
for c = 0:max(inputTable_both.compton) 
    if c <= 2
        compton_grp = inputTable_both.compton == c;
        inputTableFilt_back = inputTable_back(compton_grp, :);
        inputTableFilt_front = inputTable_front(compton_grp, :);
        inputTableFilt_both = inputTable_both(compton_grp, :);

        h = depthError_allDet(inputTableFilt_back, ...
            inputTableFilt_front, inputTableFilt_both, thickness);
        savefig(h, ['depth_error_compton_' num2str(c)])
        
        [outputStats] = detectorstats(inputTableFilt_back, ...
            num_events);
        writetable(outputStats, ['detectorStats_back_comp_' ...
            num2str(c)], 'WriteRowNames', true)
        [outputStats] = detectorstats(inputTableFilt_front, ...
            num_events);
        writetable(outputStats, ['detectorStats_front_comp_' ...
            num2str(c)], 'WriteRowNames', true)
        [outputStats] = detectorstats(inputTableFilt_both, ...
            num_events);
        writetable(outputStats, ['detectorStats_both_comp_' ...
            num2str(c)], 'WriteRowNames', true)
        
        clf(1); clf(2); clf(3);
    else        
        compton_grp = inputTable_both.compton >= c;
        inputTableFilt_back = inputTable_back(compton_grp, :);
        inputTableFilt_front = inputTable_front(compton_grp, :);
        inputTableFilt_both = inputTable_both(compton_grp, :);
        
        h = depthError_allDet(inputTableFilt_back, ...
            inputTableFilt_front, inputTableFilt_both, thickness);
        savefig(h, 'depth_error_compton_3plus')
        
        [outputStats] = detectorstats(inputTableFilt_back, ...
            num_events);
        writetable(outputStats, 'detectorStats_back_comp_3plus', ...
            'WriteRowNames', true)
        [outputStats] = detectorstats(inputTableFilt_front, ...
            num_events);
        writetable(outputStats, 'detectorStats_front_comp_3plus', ...
            'WriteRowNames', true)
        [outputStats] = detectorstats(inputTableFilt_both, ...
            num_events);
        writetable(outputStats, 'detectorStats_both_comp_3plus', ...
            'WriteRowNames', true)
        
        clf(1); clf(2); clf(3);
        break
    end   
end

% Overall error stats and tables for all data.
h = depthError_allDet(inputTable_back, inputTable_front, ...
    inputTable_both, thickness);
savefig(h, 'depth_error_compton_all')

[outputStats, outputComp] = detectorstats(inputTable_back, num_events);
writetable(outputComp, 'comptonComp_back_all.txt', 'WriteRowNames', true)
writetable(outputStats, 'detectorStats_back_all.txt', ...
    'WriteRowNames', true)
[outputStats, outputComp] = detectorstats(inputTable_front, num_events);
writetable(outputComp, 'comptonComp_front_all.txt', 'WriteRowNames', true)
writetable(outputStats, 'detectorStats_front_all.txt', ...
    'WriteRowNames', true)
[outputStats, outputComp] = detectorstats(inputTable_both, num_events);
writetable(outputComp, 'comptonComp_both_all.txt', 'WriteRowNames', true)
writetable(outputStats, 'detectorStats_both_all.txt', ...
    'WriteRowNames', true)

close all
end