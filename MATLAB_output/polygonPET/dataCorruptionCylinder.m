%% Data corruption for cylindrical PET scanner
% Want to convert the cartesian based values in the data to cylindrical
% coordinates for the scanner. Note that the Z plane in the monolith data 
% corresponds to the X plane in the cylindrical system. "refFile" is the
% .txt file from the photonfit output, thickness is in mm (not including
% SiPMs), radius _inner is distance from center of the cylinder to the
% apothem of the inner polygon.

function [output] = dataCorruptionCylinder(refFile, hitsFile, ...
    thickness, innerRadius)

% Load data files.
delimiterIn = ',';
headerlinesIn = 1;
refFileStructure = importdata(refFile, delimiterIn, headerlinesIn);

hitsFileStructure = readtable(hitsFile);

numBins = round(thickness); % Rounded to nearest inter.
binWidth = thickness/numBins;

% Bin the Z data into numBin number of bins - the bin will be parallel to 
% the detector surfaces, but for a cylindrical detector the bin will be at 
% a certain radius (approximately), so as to correspond to the curve of the
% detector surfaces. So we will say that the error in Z data will be
% approximately equal to the error in the r dimension (which will be in the
% X-Y plane).

% The r values are found with respect to the center of the cylinder.
r_real = refFileStructure.data(:, 11) + innerRadius + thickness/2;
r_meas = refFileStructure.data(:, 4) + innerRadius + thickness/2;
r_err = r_meas - r_real;

% Theta values are in radians. Converted from X-Y coordinated (though
% remember X in cylindrical system equals Z in monolith reference data).
theta_real = atan(refFileStructure.data(:, 12)./r_real);
theta_meas = atan(refFileStructure.data(:, 3)./r_meas);
theta_err = theta_real - theta_meas;

% Also get the Z error for the cylinder, which will equal X error for the
% reference monolith.
z_real = refFileStructure.data(:, 13);
z_meas = refFileStructure.data(:, 2);
z_err = z_real - z_meas;

errStats = zeros(numBins, 7); % To be filled with error stat data.

% Loop through each bin and get mean and standard dev. of r and theta.
for n = 1:numBins
    
    % Filtering data for values within the bin only.
    d = r_real >= innerRadius + (n-1)*binWidth ...
        & r_real < innerRadius + n*binWidth;
    
    r_meanError = mean(r_err(d));
    r_stdError = std(r_err(d));
    
    theta_meanError = mean(theta_err(d));
    theta_stdError = std(theta_err(d));
    
    z_meanError = mean(z_err(d));
    z_stdError = std(z_err(d));
    
    errStats(n, 1) = n;
    errStats(n, 2) = r_meanError;
    errStats(n, 3) = r_stdError;
    errStats(n, 4) = theta_meanError;
    errStats(n, 5) = theta_stdError;
    errStats(n, 6) = z_meanError;
    errStats(n, 7) = z_stdError;
   
end

figure(1)
p1 = plot(errStats(:, 1), errStats(:, 2), 'm.');
hold on
p2 = plot(errStats(:, 1), errStats(:, 2) + errStats(:, 3), 'gx');
plot(errStats(:, 1), errStats(:, 2) - errStats(:, 3), 'gx')
xlabel('bin number')
ylabel('Error r (mm)')
title('Error r dimension')
legend([p1 p2], 'mean', 'std')
hold off
    
figure(2)
p1 = plot(errStats(:, 1), errStats(:, 4), 'm.');
hold on
p2 = plot(errStats(:, 1), errStats(:, 4) + errStats(:, 5), 'gx');
plot(errStats(:, 1), errStats(:, 4) - errStats(:, 5), 'gx')
xlabel('bin number')
ylabel('Error \theta (rad)')
title('Error \theta dimension')
legend([p1 p2], 'mean', 'std')
hold off

figure(3)
p1 = plot(errStats(:, 1), errStats(:, 6), 'm.');
hold on
p2 = plot(errStats(:, 1), errStats(:, 6) + errStats(:, 7), 'gx');
plot(errStats(:, 1), errStats(:, 6) - errStats(:, 7), 'gx')
xlabel('bin number')
ylabel('Error z (mm)')
title('Error z dimension')
legend([p1 p2], 'mean', 'std')
hold off

% Convert (x,y,z) coords in the hits data to (r,theta,z), before applying
% the error.
x_cyl = hitsFileStructure.Var14;
y_cyl = hitsFileStructure.Var15;
z_cyl = hitsFileStructure.Var16;

[theta_cyl, r_cyl, z_cyl] = cart2pol(x_cyl, y_cyl, z_cyl);

% data_corruption matrix will be used to correspond coordinate data with 
% the appropriate error. These will then both be used to corrupt to data to
% a new position, based on that particular error.
data_corruption = zeros(size(r_cyl, 1), 13);

data_corruption(:, 1) = r_cyl;
data_corruption(:, 2) = theta_cyl;
data_corruption(:, 3) = z_cyl;

for m = 1:numBins
    
    % Select all data in a particular bin, then assign r,theta,z error 
    % stats.
    e = data_corruption(:, 1) >= innerRadius + (m-1)*binWidth ...
        & data_corruption(:, 1) < innerRadius + m*binWidth;
    
    data_corruption(e, 4) = m; % bin number
    data_corruption(e, 5) = errStats(m, 2); % r mean error
    data_corruption(e, 6) = errStats(m, 3); % r std error
    data_corruption(e, 7) = errStats(m, 4); % theta mean error
    data_corruption(e, 8) = errStats(m, 5); % theta std error
    data_corruption(e, 9) = errStats(m, 6); % z mean error
    data_corruption(e, 10) = errStats(m, 7); % z std error
    
end 

for i = 1:size(data_corruption,1)
    
    % Add a randomized error for r,theta,z.
    data_corruption(i, 11) = data_corruption(i, 1) ...
        + data_corruption(i, 6)*randn(1) + data_corruption(i, 5);
    
    data_corruption(i, 12) = data_corruption(i, 2) ...
        + data_corruption(i, 8)*randn(1) + data_corruption(i, 7);
    
    data_corruption(i, 13) = data_corruption(i, 3) ...
        + data_corruption(i, 10)*randn(1) + data_corruption(i, 9);
end

% Convert back to cartesian coords.
[x_corr, y_corr, z_corr] = pol2cart(data_corruption(:, 12), ...
    data_corruption(:, 11), data_corruption(:, 13));

figure(4)
scatter3(x_cyl, y_cyl, z_cyl, 'b.')
hold on
scatter3(x_corr, y_corr, z_corr, 'r.')

output = table(x_cyl, y_cyl, z_cyl, x_corr, y_corr, z_corr, ...
    'VariableNames',{'X', 'Y', 'Z', 'X_c', 'Y_c', 'Z_c'});
end