%% Number of gamma interactions in scintillator slab
% Input the number of events in each .dat Hits file and the name of the
% .txt file output from concatData/photonfit. 
% Script will find the number of primary interactions in each event,
% concatenate all data and add it as a variable in resultsTable 
% (as 'interactions').

function [resultsTable, output] = scatterCount(num_primaries, resultsTableName, ...
    outputFileName, scint_thickness)

scint_thickness = scint_thickness + 0.3;

resultsTable = readtable(resultsTableName);

% Directory to find all .dat files
currentDir = pwd;
ds = datastore([currentDir '/*.dat'], 'FileExtensions','.dat', 'Type', 'tabulartext');

% Initial conditions for loop.
event_final = 0;
output = table;

for i = 1:numel(ds.Files)
    
    filename = ds.Files{i,1};
    
    % Load Hits data file.
    delimiterIn = ',';
    headerlinesIn = 1;
    dataStructure = importdata (filename, delimiterIn, headerlinesIn);

    % % Load table of photonfit output (for the same Hits data)
    primaries_idx = dataStructure.data(:,5) == 0;
    primaries = dataStructure.data(primaries_idx, :);
    
    % Filter the primaries, so we are only looking at interactions inside
    % the crystal.
    filt_inside_crys = primaries(:, 8) >= - scint_thickness / 2 & ...
        primaries(:, 8) <= scint_thickness / 2 & ...
        primaries(:, 9) >= -10 & primaries(:, 9) <= 10 & ...
        primaries(:, 10) >= -10 & primaries(:, 10) <= 10;
    
    primaries = primaries(filt_inside_crys, :);  
    
    file_output = zeros(max(primaries(:, 4)), 4);

    for event = 1:max(primaries(:, 4)) + 1
        
        disp(['file:' int2str(i) ' event:' int2str(event)])
        
        primaries_event_idx = primaries(:, 4) == event -1;
        num_interactions = sum(primaries_event_idx);
        
        event_overall = event + num_primaries * (i - 1);
        
        if num_interactions == 0 || ~ismember(event_overall, resultsTable.event)
            continue
        end

        num_comptons = max(primaries(primaries_event_idx, 11));

        energy_deposited = sum(primaries(primaries_event_idx, 2));
        
        file_output(event, 1) = event;
        file_output(event, 2) = num_interactions;
        file_output(event, 3) = num_comptons;
        file_output(event, 4) = energy_deposited;
    end

    file_output = array2table(file_output,...
        'VariableNames',{'event', 'num_interactions', 'num_comptons', ...
        'energy_deposited'});

    % to cut out zero entries in the data set
    file_output(file_output.event == 0,:) = [];

    % Change the event number to pick up from previous output.
    file_output.event = file_output.event + event_final;
    event_final = num_primaries * i; 
    
    % Concatenate new table with previous.
    output = vertcat(output, file_output);
end

resultsTable = addvars(resultsTable, output.num_interactions, ...
    output.energy_deposited, 'After', 'compton', 'NewVariableNames', ...
    {'interactions' 'energyDeposited'});

% Save output table.
writetable(resultsTable, outputFileName)

end