% Script to seperate a photonfit output table into groups depending on the
% number of compton scatters, then apply the depthError function to each
% (i.e. X,Y,Z error with variable Z). Also an overall depthError will be 
% done with all data.

function depthError_numComptons(inputTable, thickness, num_events)

% set plots to Latex interpreter.
set(groot,'defaulttextinterpreter','latex');  
set(groot, 'defaultAxesTickLabelInterpreter','latex');  
set(groot, 'defaultLegendInterpreter','latex');

% Split to input table into sub-tables, depending on the number of compton
% scatters. Save output plots of X,Y,Z error, binned with varying Z. (1mm
% bins). Photoelectrics and compton scatter (1-4) are plotted individually.
% Due to their lower number, comptons 5+ are grouped together. 
for c = 0:max(inputTable.compton) 
    if c <= 4
        compton_grp = inputTable.compton == c;
        inputTableFilt = inputTable(compton_grp, :);

        h = depthError(inputTableFilt, thickness);
        savefig(h, ['depth_error_compton_' num2str(c)])
        
        [outputStats] = detectorstats(inputTableFilt, num_events);
        writetable(outputStats, ['detectorStats_comp_' num2str(c)])
    else        
        compton_grp = inputTable.compton >= c;
        inputTableFilt = inputTable(compton_grp, :);
        
        h = depthError(inputTableFilt, thickness);
        savefig(h, 'depth_error_compton_5plus')
        
        [outputStats] = detectorstats(inputTableFilt, num_events);
        writetable(outputStats, 'detectorStats_comp_5plus')        
        break       
    end   
end

% Overall error stats and tables for all data.
h = depthError(inputTable, thickness);
savefig(h, 'depth_error_compton_all')

[outputStats, outputComp] = detectorstats(inputTable, num_events);
writetable(outputStats, 'detectorStats_all.txt')
writetable(outputComp, 'comptonComp_all.txt')

close all
end