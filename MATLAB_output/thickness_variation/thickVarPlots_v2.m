%% Plots for scintillator thickness variation within certain error (Del_d)
% this script needs to be in the directory 3 levels above the data files to
% work correctly.

function [output_nano, output_cer] = thickVarPlots_v2(error_mm, num_events)

% set plots to Latex interpreter.
set(groot,'defaulttextinterpreter','latex');  
set(groot, 'defaultAxesTickLabelInterpreter','latex');  
set(groot, 'defaultLegendInterpreter','latex');

% -------------------Nanocomps Table---------------------------------------
% Defined to access data directories
thickness = 0:10:70;
materials = {'Gd2O3_PVT', 'LaBr3Ce_PS_7', 'LaF3Ce_OA_12', 'LaF3Ce_PS', ...
    'YAGCe_PS_37'};

% Preallocated space to input all data files. Add an extra column for 
% (0, 0) point.
data = zeros(size(materials, 2), size(thickness, 2));

% Reading in data, then saving total error values to the preallocated cell.
for i = 1:size(materials, 2)
    for j = 2:size(thickness, 2)

        file = dir(fullfile('.', 'nanocomps', [materials{1, i} '_artFix'], ...
            [num2str(thickness(j)) 'mm'], '*.txt'));
        inputTable = readtable(fullfile(file.folder, file.name));
        
        table_filtered = inputTable(inputTable.Del_d <= error_mm, :);
        percentage_detections = size(table_filtered, 1) / num_events * 100;
        
        data(i, j) = percentage_detections; 
    end 
end

% Fit a quadratic to the datapoints, then plot
x = linspace(0, 80);
colour = {'c', 'r', 'm', 'g', 'b'};
marker = {'o', '+', '*', '^', 'x'};
lm = cell(1, 5); % markers for the legend.
max_values = zeros(5, 2);

for n = 1:size(data, 1)
    p = polyfit(thickness, data(n, :), 2);
    y = polyval(p, x);

    figure(1)
    hold on

    plot(x, y, colour{n});
    s = scatter(thickness, data(n, :), [colour{n} marker{n}]);
    
    lm{n} = s;
    
    % Find thickness at max value on the curves
    vert_x = -p(2) / (2 * p(1)); % Thickness at vertex.
    vert_y = p(1) * vert_x ^ 2 + p(2) * vert_x + p(3); % Events at vertex.
    
    max_values(n, 1) = vert_x;
    max_values(n, 2) = vert_y;
end

grid on
hold off
xlabel('Scintillator Thickness (mm)')
ylabel('Events Detected (\%)')
ylim([0 100])
legend([lm{1} lm{2} lm{3} lm{4} lm{5}], {'Gd2O3 PVT', 'LaBr3:Ce PS', ...
    'LaF3:Ce OA', 'LaF3:Ce PS', 'YAG:Ce PS'}, 'location', 'northwest')

% Create table for output
output_nano = array2table(max_values, 'RowNames', {'Gd2O3_PVT', ...
    'LaBr3Ce_PS_7', 'LaF3Ce_OA_12', 'LaF3Ce_PS', 'YAGCe_PS_37'}, ...
    'VariableNames', {'Thickness', 'Events'});

%---------------------------Ceramics---------------------------------------
% Defined to access data directories
thickness = 0:5:35;
materials = {'GAGG', 'GLuGAG', 'GYGAG', 'LuAG'};

% Preallocated space to input all data files.
data = zeros(size(materials, 2), size(thickness, 2));

% Reading in data, then saving total error values to the preallocated cell.
for i = 1:size(materials, 2)
    for j = 2:size(thickness, 2)

        file = dir(fullfile('.', 'ceramics', [materials{1, i} '_artFix'], ...
            [num2str(thickness(j)) 'mm'], '*.txt'));
        inputTable = readtable(fullfile(file.folder, file.name));
        
        table_filtered = inputTable(inputTable.Del_d <= error_mm, :);
        percentage_detections = size(table_filtered, 1) / num_events * 100;
        
        data(i, j) = percentage_detections; 
    end 
end

% Fit a quadratic to the datapoints, then plot
x = linspace(0, 40);
max_values = zeros(4, 2);

for n = 1:size(data, 1)
    p = polyfit(thickness, data(n, :), 2);
    y = polyval(p, x);

    figure(2)
    hold on

    plot(x, y, colour{n});
    s = scatter(thickness, data(n, :), [colour{n} marker{n}]);
    
    lm{n} = s;
    
    % Find thickness at max value on the curves
    vert_x = -p(2) / (2 * p(1)); % Thickness at vertex.
    vert_y = p(1) * vert_x ^ 2 + p(2) * vert_x + p(3); % Events at vertex.
    
    max_values(n, 1) = vert_x;
    max_values(n, 2) = vert_y;
end

grid on
hold off
xlabel('Scintillator Thickness (mm)')
ylabel('Events Detected (\%)')
ylim([0 100])
legend([lm{1} lm{2} lm{3} lm{4}],{'GAGG:Ce', 'GLuGAG:Ce', 'GYGAG:Ce', ...
    'LuAG:Pr'}, 'location', 'northwest')

% Create table for output
output_cer = array2table(max_values, 'RowNames', {'GAGG', 'GLuGAG', ...
    'GYGAG', 'LuAG'}, 'VariableNames', {'Thickness', 'Events'});

end