%% Plots for thickness variation for a single scintillator material.
% All error (Del_d) values are plotted, with fit by cubic spline.
% Choose class as either 'nanocomps' or 'ceramics'.
% Choose material from: 'GAGG', 'GLuGAG', 'GYGAG', 'LuAG', 'Gd2O3_PVT',
% 'LaBr3Ce_PS_7', 'LaF3Ce_OA_12', 'LaF3Ce_PS', 'YAGCe_PS_37'.

function [outputTable] = thickVarPlots_spline_errBars(num_events, num_data_splits, class, material)

% set plots to Latex interpreter.
set(groot,'defaulttextinterpreter','latex');  
set(groot, 'defaultAxesTickLabelInterpreter','latex');  
set(groot, 'defaultLegendInterpreter','latex');

% Access data directory
if strcmp(class, 'nanocomps')
    thickness = 0:10:70;
elseif strcmp(class, 'ceramics') && strcmp(material, 'GYGAG')
    thickness = 0:5:45;
elseif strcmp(class, 'ceramics')
    thickness = 0:5:35;
end

Del_D_mm = [1:5 inf];

% Preallocated space to input all data files. Add an extra column for 
% (0, 0) point.
data_detections_split = zeros(num_data_splits, 1);
data_mean = zeros(size(Del_D_mm, 2), size(thickness, 2));
data_err = zeros(size(Del_D_mm, 2), size(thickness, 2));

% Reading in data, then saving total error values to the preallocated cell.
for i = 1:size(Del_D_mm, 2)
    for j = 2:size(thickness, 2)
        file = dir(fullfile('.', class, [material '_artFix'], ...
            [num2str(thickness(j)) 'mm'], '*.txt'));
        inputTable = readtable(fullfile(file.folder, file.name));
        
        % Splitting the data up to find mean and std error (for error bars)
        for k = 1:num_data_splits      
            split = inputTable.event >= (num_events / num_data_splits) * (k - 1) + 1 ...
                & inputTable.event <= (num_events / num_data_splits) * k;
            data_split = inputTable(split, :);
            
            table_filtered = data_split(data_split.Del_d <= Del_D_mm(i), :);
            percentage_detections = size(table_filtered, 1) / (num_events / num_data_splits) * 100;
            
            data_detections_split(k) = percentage_detections;
        end
        % Calculate mean and std error of all the data, then store in
        % preallocated arrays.
        mean_split = mean(data_detections_split);
        std_err_split = std(data_detections_split) / sqrt(length(data_detections_split));
        
        data_mean(i, j) = mean_split;
        data_err(i,j) = std_err_split;
    end 
end

% Fit a quadratic to the datapoints, then plot
if strcmp(class, 'nanocomps')
    x = linspace(0, 70);
elseif strcmp(class, 'ceramics') && strcmp(material, 'GYGAG')
    x = linspace(0, 45);
elseif strcmp(class, 'ceramics')
    x = linspace(0, 35);
end

colour = {'c', 'r', 'm', 'g', 'b', 'k'};
marker = {'o', '+', '*', '^', 'x', 's'};
lm = cell(1, size(data_mean, 1)); % markers for the legend.
max_values = zeros(size(data_mean, 1), 2);

for n = 1:size(data_mean, 1)
    sp = spline(thickness, data_mean(n, :));
    
    figure(1)
    hold on
    p = plot(x, ppval(sp,x), [colour{n} '-']);
    %s = scatter(thickness, data_mean(n, :), [colour{n} '.']);%marker{n}]);
    errorbar(thickness, data_mean(n, :), data_err(n, :), 'LineStyle', 'none');
    
    lm{n} = p;
    
    % Find turning point, where the first derviative == 0.
    vert_x = fnzeros(fnder(sp));
    vert_y = fnval(sp,fnzeros(fnder(sp)));
    
    if ~isempty(vert_x) && ~isempty(vert_y)
        max_values(n, 1) = vert_x(1);
        max_values(n, 2) = vert_y(2);
    else
        max_values(n, 1) = NaN;
        max_values(n, 2) = NaN;
    end
end

grid on
hold off
xlabel('Scintillator Thickness (mm)')
ylabel('Events Detected (\%)')
ylim([0 100])
legend([lm{1} lm{2} lm{3} lm{4} lm{5} lm{6}], {'$\Delta$D $\leq$ 1mm', ...
    '$\Delta$D $\leq$ 2mm', '$\Delta$D $\leq$ 3mm', '$\Delta$D $\leq$ 4mm', ...
    '$\Delta$D $\leq$ 5mm', 'All Detections'}, 'location', 'northwest')

% Creating output table.
outputTable = array2table(max_values, 'RowNames', {'1mm', '2mm', '3mm', ...
    '4mm', '5mm', 'Inf'}, 'VariableNames', {'Thickness', 'Max_Events'});

pause;
close all;

end