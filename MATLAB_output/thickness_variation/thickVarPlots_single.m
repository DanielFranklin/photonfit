%% Plots for thickness variation for a single scintillator material.
% All error (Del_d) values are plotted.
% Choose class as either 'nanocomps' or 'ceramics'.
% Choose material from: 'GAGG', 'GLuGAG', 'GYGAG', 'LuAG', 'Gd2O3_PVT',
% 'LaBr3Ce_PS_7', 'LaF3Ce_OA_12', 'LaF3Ce_PS', 'YAGCe_PS_37'.

function thickVarPlots_single(num_events, class, material)

% set plots to Latex interpreter.
set(groot,'defaulttextinterpreter','latex');  
set(groot, 'defaultAxesTickLabelInterpreter','latex');  
set(groot, 'defaultLegendInterpreter','latex');

% Access data directory
if strcmp(class, 'nanocomps')
    thickness = 0:10:70;
elseif strcmp(class, 'ceramics')
    thickness = 0:5:35;
end

error_mm = [1:5 inf];
% Preallocated space to input all data files. Add an extra column for 
% (0, 0) point.
data = zeros(size(error_mm, 2), size(thickness, 2));

% Reading in data, then saving total error values to the preallocated cell.
for i = 1:size(error_mm, 2)
    for j = 2:size(thickness, 2)

        file = dir(fullfile('.', class, [material '_artFix'], ...
            [num2str(thickness(j)) 'mm'], '*.txt'));
        inputTable = readtable(fullfile(file.folder, file.name));
        
        table_filtered = inputTable(inputTable.Del_d <= error_mm(i), :);
        percentage_detections = size(table_filtered, 1) / num_events * 100;
        
        data(i, j) = percentage_detections; 
    end 
end

% Fit a quadratic to the datapoints, then plot
if strcmp(class, 'nanocomps')
    x = linspace(0, 80);
elseif strcmp(class, 'ceramics')
    x = linspace(0, 40);
end

colour = {'c', 'r', 'm', 'g', 'b', 'k'};
marker = {'o', '+', '*', '^', 'x', 's'};
lm = cell(1, size(data, 1)); % markers for the legend.
max_values = zeros(5, 2);

for n = 1:size(data, 1)
    p = polyfit(thickness, data(n, :), 2);
    y = polyval(p, x);

    figure(1)
    hold on

    plot(x, y, colour{n});
    s = scatter(thickness, data(n, :), [colour{n} marker{n}]);
    
    lm{n} = s;
    
    % Find thickness at max value on the curves
    vert_x = -p(2) / (2 * p(1)); % Thickness at vertex.
    vert_y = p(1) * vert_x ^ 2 + p(2) * vert_x + p(3); % Events at vertex.
    
    max_values(n, 1) = vert_x;
    max_values(n, 2) = vert_y;
end

grid on
hold off
xlabel('Scintillator Thickness (mm)')
ylabel('Events Detected (\%)')
ylim([0 100])
legend([lm{1} lm{2} lm{3} lm{4} lm{5} lm{6}], {'1mm', '2mm', ...
    '3mm', '4mm', '5mm', 'Inf'}, 'location', 'northwest')

end