%% Plot thickness variation for nanocomps and ceramics
% Input .csv files

function thickVarPlots(ceramic_file,nanocomp_file)
% set plots to Latex interpreter.
set(groot,'defaulttextinterpreter','latex');  
set(groot, 'defaultAxesTickLabelInterpreter','latex');  
set(groot, 'defaultLegendInterpreter','latex');

ceramic_data = csvread(ceramic_file,2,0);
nanocomp_data = csvread(nanocomp_file,2,0);

% Seperate data into seperate materials. Scatter plot for each and use
% polyfit fn. to fit a second order polynomial to the points.

% -------------------------------------------------------------------------
GAGG_data = ceramic_data(:,1:4);
GLuGAG_data = ceramic_data(:,[1 5:7]);
GYGAG_data = ceramic_data(:,[1 8:10]);
LuAG_data = ceramic_data(:,[1 11:13]);

x = linspace(0, 40);
p1 = polyfit(GAGG_data(:,1),GAGG_data(:,3),2);
y1 = polyval(p1,x);

p2 = polyfit(GLuGAG_data(:,1),GLuGAG_data(:,3),2);
y2 = polyval(p2,x);

p3 = polyfit(GYGAG_data(:,1),GYGAG_data(:,3),2);
y3 = polyval(p3,x);

p4 = polyfit(LuAG_data(:,1),LuAG_data(:,3),2);
y4 = polyval(p4,x);

figure(1)
hold on

s1 = scatter(GAGG_data(:,1),GAGG_data(:,3),'ro');
s2 = scatter(GLuGAG_data(:,1),GLuGAG_data(:,3),'b+');
s3 = scatter(GYGAG_data(:,1),GYGAG_data(:,3),'g*');
s4 = scatter(LuAG_data(:,1),LuAG_data(:,3),'m^');

plot(x,y1,'r');
plot(x,y2,'b');
plot(x,y3,'g');
plot(x,y4,'m');

grid on
hold off
xlabel('Scintillator Thickness (mm)')
ylabel('Events Detected (\%)')
ylim([0 100])
legend([s1 s2 s3 s4],{'GAGG:Ce','GLuGAG:Ce','GYGAG:Ce','LuAG:Pr'}, ...
    'location','northwest')

% Do the same for nanocomposites.
% -------------------------------------------------------------------------
Gd2O3_PVT_data = nanocomp_data(:,1:4);
LaBr3Ce_PS_data = nanocomp_data(:,[1 5:7]);
LaF3Ce_OA_data = nanocomp_data(:,[1 8:10]);
LaF3Ce_PS_data = nanocomp_data(:,[1 11:13]);
YAGCe_PS_data = nanocomp_data(:,[1 14:16]);

x = linspace(0, 80);
p1 = polyfit(Gd2O3_PVT_data(:,1),Gd2O3_PVT_data(:,3),2);
y1 = polyval(p1,x);

p2 = polyfit(LaBr3Ce_PS_data(:,1),LaBr3Ce_PS_data(:,3),2);
y2 = polyval(p2,x);

p3 = polyfit(LaF3Ce_OA_data(:,1),LaF3Ce_OA_data(:,3),2);
y3 = polyval(p3,x);

p4 = polyfit(LaF3Ce_PS_data(:,1),LaF3Ce_PS_data(:,3),2);
y4 = polyval(p4,x);

p5 = polyfit(YAGCe_PS_data(:,1),YAGCe_PS_data(:,3),2);
y5 = polyval(p5,x);

figure(2)
hold on

s1 = scatter(Gd2O3_PVT_data(:,1),Gd2O3_PVT_data(:,3),'co');
s2 = scatter(LaBr3Ce_PS_data(:,1),LaBr3Ce_PS_data(:,3),'r+');
s3 = scatter(LaF3Ce_OA_data(:,1),LaF3Ce_OA_data(:,3),'m*');
s4 = scatter(LaF3Ce_PS_data(:,1),LaF3Ce_PS_data(:,3),'g^');
s5 = scatter(YAGCe_PS_data(:,1),YAGCe_PS_data(:,3),'bx');

plot(x,y1,'c');
plot(x,y2,'r');
plot(x,y3,'m');
plot(x,y4,'g');
plot(x,y5,'b');

grid on
hold off
xlabel('Scintillator Thickness (mm)')
ylabel('Events Detected (\%)')
ylim([0 100])
legend([s1 s2 s3 s4 s5],{'Gd2O3 PVT','LaBr3:Ce PS','LaF3:Ce OA','LaF3:Ce PS'...
    'YAG:Ce PS'},'location','northwest')

end