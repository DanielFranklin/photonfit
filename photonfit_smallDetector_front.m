% det_dim_pix and det_dim_mm are 2-element vectors of the detector
% dimensions in pixels and mm, respectively.
% 
% For the sim we are currently using, the slab is 2 cm square (i.e.  20
% mm).  It seems that about 10 pixels is reasonable. So call this as
% photonfit ('Hits.dat', [10 10], [20 20]);
%
% The axes of the subfigures are linked together; move one and you move the
% other too.
%
% fastmode skips drawing figures. Active/deactive as 1/0.

function [output] = photonfit_smallDetector_front (filename, det_dim_pix, det_dim_mm, fastmode)

% Load data file.
delimiterIn = ',';
headerlinesIn = 1;
dataStructure = importdata (filename, delimiterIn, headerlinesIn);

% Generate an XY grid representing the bounds of the detector.
half_dim_mm = det_dim_mm / 2;
step = det_dim_mm ./ (det_dim_pix - 1);

[X, Y] = meshgrid((0 : step(1) : det_dim_mm(1)) - det_dim_mm(1) / 2, ...
    (0 : step(2) : det_dim_mm(2)) - det_dim_mm (2) / 2);

grid_coords = zeros (det_dim_pix (1), det_dim_pix (2), 2);
grid_coords (:, :, 1) = X; 
grid_coords (:, :, 2) = Y;

% Filter out the optical photons from the main dataset, include only rows
% that specify their position of detection and the event ID. (Column 5 is
% parent ID and column 3 is pixel ID.)
primary_secondary_rows = find (dataStructure.data (:,8) == -5.15 | ...
    dataStructure.data (:,5) == 0);
% Keep only event and parent ID's and X,Y,Z positions.
allData = dataStructure.data(primary_secondary_rows, [4:5 8:10]);

% Split data up into photons hitting the front and the back detectors.
optical_rows = find (dataStructure.data (:,5) == 1 & dataStructure.data (:,8) == -5.15);
opticalHits = dataStructure.data (optical_rows, [4 8:11]);

% Create a 3D matrix Z where each slice will be filled with the hit data
% from a single event.
Z = zeros (det_dim_pix (1), det_dim_pix (2), max (opticalHits (:,1)) + 1);

% create an array that will be populated with the mean squared error for
% each event.
output = zeros(max(opticalHits(:,1)) + 1, 17);

% parameters for the tree figures.
colour = ['g', 'b', 'c', 'm', 'y', 'k', 'w'];
ncolours = length (colour);

figure (1);

%% --- Function Fitting ---

% Options for lsqcurvefit
if fastmode
    options = optimoptions('lsqcurvefit','Algorithm','levenberg-marquardt', 'MaxIter', 1000, 'MaxFunEvals', 5000, 'TolFun', 1e-20, 'TolX', 1e-20);
else
    options = optimoptions('lsqcurvefit','Algorithm','levenberg-marquardt', 'Display', 'iter-detailed', 'MaxIter', 1000, 'MaxFunEvals', 5000, 'TolFun', 1e-20, 'TolX', 1e-20);
end

lb = [];
ub = [];

atten_length = 59.7;

objfunc = gen_psa_args (atten_length);


for event = 1:max(opticalHits(:,1)) + 1;
	
	% Filtering optical hits by event ID.
	opticalFilt = opticalHits (ismember (opticalHits(:, 1), event - 1), :); 
    
    if ~isempty(opticalFilt)
	
	    % Populating Z with optical hits on the XY plane. Each slice of Z is a
	    % different event. Then plot.
	    Z (:, :, event) = hist3 ([opticalFilt(:, 3) opticalFilt(:, 4)], det_dim_pix);
        
        if fastmode ~= 1
            ax1 = subplot (2, 2, 1);
            mesh (X, Y, Z (:, :, event));
            axis tight;
        end

        % Starting guess of x, y, z = (0, 0, 25), zero attenuation and total number
        % of photons in the burst is assumed to be the total number seen on the
        % detector (we lose a few but should be fairly close).  See
        % pointsourceattenuative.m for details.
        
        % The initial parameters have been changed to randomized values to
        % avoid local minima. Positions have been confined to an interval
        % based on the physical size of the scintillator. For interval
        % (a,b), r_i = a + (a - b).*rand(N).
        % for loop calculates the parameters i times, then compares the resnorm of
        % each and takes the values corresponding to the lowest.
        for i = 1:5
            params = [];
            resnorm = [];
            residual = [];
            exitflag = [];
        
            x_i = -10 + (10 + 10).*rand(1);
            y_i = -10 + (10 + 10).*rand(1);
            z_i = -5 + (5 + 5).*rand(1);

            initial_params = [x_i, y_i, z_i, 0, sum(sum (Z (:, :, event)))]

%            [calc_params, calc_resnorm, calc_residual, calc_exitflag] = lsqcurvefit (@pointsourceattenuative, ...
%                initial_params, grid_coords, Z (:, :, event), lb, ub, options);

            [calc_params, calc_resnorm, calc_residual, calc_exitflag] = lsqcurvefit (objfunc, ...
                initial_params, grid_coords, Z (:, :, event), lb, ub, options);

            if isempty(params);
               params = calc_params;
               resnorm = calc_resnorm;
               residual = calc_residual;
               exitflag = calc_exitflag;

            elseif calc_resnorm < resnorm; 
               params = calc_params;
               resnorm = calc_resnorm;
               residual = calc_residual;
               exitflag = calc_exitflag;
            end
        end
        
        % To test if the paramaters lie within the volume of the
        % scintillator.
        if params(1) < -10 || params(1) > 10 || params(2) < -10 || params(2) > 10 || params(3) < 0 || params(3) > 10.2
            continue
        end
	
	    % Solution to the gaussian (with calculated parameters) used in the
	    % calculation of the MSE. (**function no longer gaussian**)
%	    G = D2GaussFunctionRot(params,grid_coords);
%	    G = pointsourceattenuative (params, grid_coords);
            G = pointsourceattenuative_args (params, grid_coords, atten_length);

        if fastmode ~= 1
            ax2 = subplot (2, 2, 2);
            mesh (X, Y, G);
            axis tight;
            
            disp (sprintf ('Estimated x, y, z: %f %f %f', params (1), params (2), params (3)));
            disp (sprintf ('Estimated yield: %f photons/primary (this is probably dodgy)', params (5)));
        end
        
        % Link the axes of the two subfigures so we can rotate them together;
% 	    hlink = linkprop ([ax1, ax2], {'CameraPosition', 'CameraUpVector'});
% 	    rotate3d on;
        
        % Fill output matrix with event #, calculated values from
        % lscurvefit, # of compton scatters, mean squared error and
        % exitflag from lsqcurvefit (to make sure an answer is reached.
        output(event,1) = event;
        output(event,2:6) = params(1:5);
        output(event,7) = opticalFilt(end,5);
        output(event,8) = immse(Z(:,:,event), G);
        output(event,9) = resnorm;
        output(event,10) = exitflag;

    end
    
%% --- Plotting Primaries and Secondaries ---  

    x0 = -5;
    y0 = 0;
    z0 = 0;
    
    allDataFilt = allData (ismember (allData(:, 1), event - 1), :);
    
    if ~isempty(allDataFilt)
        primary_rows = find (allDataFilt(:, 2) == 0);
        secondary_rows = find (allDataFilt(:, 2) == 1);
        
        max_rays = min(20, size(secondary_rows ,1));

        % Iterate through all primaries that we can find (i.e. where parent ID = 0)
        if fastmode ~= 1
            for np = 1 : max (size (primary_rows))

                px0 = x0;
                py0 = y0;
                pz0 = z0;

                x0 = allDataFilt (primary_rows (np), 3); % posX
                y0 = allDataFilt (primary_rows (np), 4); % posY
                z0 = allDataFilt (primary_rows (np), 5); % posZ

                % Get line of first optical event
                optical = primary_rows (np) + 1;
                rays = 1;

                % only run if optical row is within the matrix size (to prevent
                % indexing errors for primaries without secondary photons.
                if optical <= size(allDataFilt,1)

                    figure(1);
                    ax3 = subplot (2, 2, 3);
                    hold on

                    % Column 5 is parent ID (0 for primary gamma rays and 1 for optical
                    % photons). So while parent ID = 1 (optical) and event ID = current_event_id
                    % (i.e. the same event) and rays are < 20 (to speed up processing),
                    % then plot X,Y,Z of each hit, then increment to next optical photon
                    % (up to 19 rays).
                    while (allDataFilt (optical, 2) == 1) && (rays < max_rays)
                        plot3 ([x0; allDataFilt(optical, 3)], [y0; allDataFilt(optical,4)], ...
                            [z0; allDataFilt(optical,5)], sprintf ('%c-*', colour (1 + mod (np, ncolours))));
                        optical = optical + 1;
                        rays = rays + 1;
                    end        
                    plot3 ([px0; x0], [py0; y0], [pz0; z0], 'r-+');
                    axis equal;
                    axis ([-5.15, 5.15, -det_dim_mm(1)/2, det_dim_mm(1)/2, -det_dim_mm(2)/2, det_dim_mm(2)]);


                    figure (1);
                    ax4 = subplot (2, 2, 4);
                    hold on
                    plot3 ([px0; x0], [py0; y0], [pz0; z0], 'r-+');
                    axis equal;
                    axis ([-5.15, 5.15, -det_dim_mm(1)/2, det_dim_mm(1)/2, -det_dim_mm(2)/2, det_dim_mm(2)]);

                end

            end
            
        end
%         hlink1 = linkprop ([ax1, ax2, ax3, ax4], {'cameraposition','cameraupvector'});
% 	    rotate3d on;
        
%         hlink2 = linkprop ([ax3, ax4], {'cameraposition','cameraupvector'});
% 	    rotate3d on;

        % Adding x,y,z position of the first point of interaction of
        % primary with scintillator (x0, y0, z0).  
        output(event,11:13) = [(allDataFilt (primary_rows (1), 3)), (allDataFilt (primary_rows (1), 4)), ...
            (allDataFilt (primary_rows (1), 5))];
        
        %Then add error between calculated values and actual position
        %(del_x, del_y, del_z). Abs. values removed.
        output(event,14:16) = [(output(event,2) - output(event,13)), (output(event,3) - output(event,12)), ...
            (output(event,4) - output(event,11) - 5.1)];
        
        % and total error (del_d).
        output(event,17) = sqrt(output(event,14).^2 + output(event,15).^2 + output(event,16).^2);
        
        if fastmode ~= 1
            pause;
        end 
        clf;

    end
end

close all;

%% --- Output result in a table ---

% Convert output into a table for data cleaning.
output = array2table(output,...
    'VariableNames',{'event', 'x', 'y', 'z', 'attenuation', 'lightYield', 'compton', 'error', 'resnorm', ...
    'exitflag', 'z0', 'y0', 'x0', 'Del_x', 'Del_y', 'Del_z', 'Del_d'})

% to cut out zero entries in the data set
output(output.event == 0,:) = [];

