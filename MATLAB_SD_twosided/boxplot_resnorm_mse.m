% Function outputs box plots to view statistical distribution of MSE and 
% resnorm for each number of compton scatters.

function [boxplotvalues_resnorm, boxplotvalues_error] = boxplot_resnorm_mse(inputTable)
%% --- Box plots and statistical calculations ---

% preallocate table to be filled by box plot data.
boxplotvalues_resnorm = zeros(max(inputTable.compton) + 1, 7);
boxplotvalues_error = zeros(max(inputTable.compton) + 1, 7);

for c = 0:max(inputTable.compton);
    rows = inputTable.compton == c;
    vars = {'resnorm','error'};
    num_compton{c+1} = inputTable(rows,vars);
    
    % Calculate boxplot values for median, Q1, Q3, low and high adjacents
    % and number of outliers. For the resnorm:
    median_res = median(num_compton{1,c+1}.resnorm);
    Q1_res = prctile(num_compton{1,c+1}.resnorm,25);
    Q3_res = prctile(num_compton{1,c+1}.resnorm,75); 
    
    IQR_res = Q3_res - Q1_res; % Interquartile range.
    
    low_thresh_res = Q1_res - (1.5*IQR_res); % lower threshold.
    possible_low_res = num_compton{1,c+1}.resnorm > low_thresh_res;
    low_adj_res = min(num_compton{1,c+1}.resnorm(possible_low_res)); % lower adjacent.
    
    high_thresh_res = Q3_res + (1.5*IQR_res); % upper threshold.
    possible_high_res = num_compton{1,c+1}.resnorm < high_thresh_res;
    high_adj_res = max(num_compton{1,c+1}.resnorm(possible_high_res)); % upper adjacent.
    
    total_outliers_res = sum(~possible_low_res) + sum(~possible_high_res);
    
    
    % For the mean squared error:
    median_mse = median(num_compton{1,c+1}.error);
    Q1_mse = prctile(num_compton{1,c+1}.error,25);
    Q3_mse = prctile(num_compton{1,c+1}.error,75);
    
    IQR_mse = Q3_mse - Q1_mse; % Interquartile range.
    
    low_thresh_mse = Q1_mse - (1.5*IQR_mse); % lower threshold.
    possible_low_mse = num_compton{1,c+1}.error > low_thresh_mse;
    low_adj_mse = min(num_compton{1,c+1}.error(possible_low_mse)); % lower adjacent.
    
    high_thresh_mse = Q3_mse + (1.5*IQR_mse); % upper threshold.
    possible_high_mse = num_compton{1,c+1}.error < high_thresh_mse;
    high_adj_mse = max(num_compton{1,c+1}.error(possible_high_mse)); % upper adjacent.
    
    total_outliers_mse = sum(~possible_low_mse) + sum(~possible_high_mse);
    
    % Insert values into output tables.
    if high_thresh_res ~= low_thresh_res
        boxplotvalues_resnorm(c+1, 1) = c;
        boxplotvalues_resnorm(c+1, 2) = low_adj_res;
        boxplotvalues_resnorm(c+1, 3) = Q1_res;
        boxplotvalues_resnorm(c+1, 4) = median_res;
        boxplotvalues_resnorm(c+1, 5) = Q3_res;
        boxplotvalues_resnorm(c+1, 6) = high_adj_res;
        boxplotvalues_resnorm(c+1, 7) = total_outliers_res;
        
    else
        boxplotvalues_resnorm(c+1, 1) = c;
        boxplotvalues_resnorm(c+1, 2) = NaN;
        boxplotvalues_resnorm(c+1, 3) = Q1_res;
        boxplotvalues_resnorm(c+1, 4) = median_res;
        boxplotvalues_resnorm(c+1, 5) = Q3_res;
        boxplotvalues_resnorm(c+1, 6) = NaN;
    end
    
    
    if high_thresh_mse ~= low_thresh_mse
        boxplotvalues_error(c+1, 1) = c;
        boxplotvalues_error(c+1, 2) = low_adj_mse;
        boxplotvalues_error(c+1, 3) = Q1_mse;
        boxplotvalues_error(c+1, 4) = median_mse;
        boxplotvalues_error(c+1, 5) = Q3_mse;
        boxplotvalues_error(c+1, 6) = high_adj_mse;
        boxplotvalues_error(c+1, 7) = total_outliers_mse;
    else
        boxplotvalues_error(c+1, 1) = c;
        boxplotvalues_error(c+1, 2) = NaN;
        boxplotvalues_error(c+1, 3) = Q1_mse;
        boxplotvalues_error(c+1, 4) = median_mse;
        boxplotvalues_error(c+1, 5) = Q3_mse;
        boxplotvalues_error(c+1, 6) = NaN;
    end
end

% Convert boxplot arrays to tables for output.
boxplotvalues_resnorm = array2table(boxplotvalues_resnorm, 'VariableNames', ...
    {'compton', 'lower_adjacent', 'Q1', 'median', 'Q3', 'upper_adjacent', 'outliers'}); 

boxplotvalues_error = array2table(boxplotvalues_error, 'VariableNames', ...
    {'compton', 'lower_adjacent', 'Q1', 'median', 'Q3', 'upper_adjacent', 'outliers'});

% Plot Boxplots
figure(1)
boxplot(inputTable.resnorm, inputTable.compton)
title('Resnorm by Number of Compton Scatters')
xlabel('Number of Compton Scatters')
ylabel('Resnorm')

figure(2)
boxplot(inputTable.error, inputTable.compton)
title('MSE by Number of Compton Scatters')
xlabel('Number of Compton Scatters')
ylabel('Mean Squared Error')
end