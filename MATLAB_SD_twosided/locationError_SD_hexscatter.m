
% Function to return a histogram of primary particel first hit location
% error, as calculated by the photonfit function. Then a scatter plot of
% error in the x,y,z directions with respect to the primary's distance from
% the detector.

function locationError_SD_hexscatter(inputTable, slab_thickness)

% Set plots to Latex interpreter.
set(groot,'defaulttextinterpreter','latex');  
set(groot, 'defaultAxesTickLabelInterpreter','latex');  
set(groot, 'defaultLegendInterpreter','latex');
% arrange into sets depending on # of compton scatters.

% filter out estimates that are not inside the scintillator volume.
filter = inputTable.x >= -10 & inputTable.x <= 10  ...
    & inputTable.y >= -10 & inputTable.y <= 10 ...
    & inputTable.z >= -(slab_thickness/2) & inputTable.z <= (slab_thickness/2);

inputTable = inputTable(filter,:);

for c = 0:max(inputTable.compton)
    rows = inputTable.compton == c;
    vars = {'z0','Del_x','Del_y','Del_z','Del_d'};
    num_compton{c+1} = inputTable(rows,vars);
    
    h(1) = figure(1)
    % Histogram of total distance error of estimated gamma location, for
    % varying number of compton scatters.
    edges = 0:0.5:20;
    histogram(num_compton{1,c+1}.Del_d, edges);
%     title(['\Delta D Histogram, compton scatters:' num2str(c)]);
    xlim([0 20]);
    xlabel('\Delta D (mm)');
    ylabel('Frequency');

    h(2) = figure(2)
    % Scatter plots for error in x,y,z position and total distance (d) vs.
    % distance from detector (Z), for varying number of comton scatters.
%     subplot (2, 2, 1)
%     scatter(num_compton{1,c+1}.z0, num_compton{1,c+1}.Del_x, '.');
    colormap (jet (1024));
    hexscatter(num_compton{1,c+1}.z0, num_compton{1,c+1}.Del_x, 'res', 100);
    axis ([-slab_thickness/2 slab_thickness/2 -10 10]);
%     title('\Delta X vs. Z');
    xlabel('Z location (mm)');
    ylabel('$\Delta$ X (mm)');
    colorbar
    grid on
    
    h(3) = figure(3)
%     subplot (2, 2, 2)
%     scatter(num_compton{1,c+1}.z0, num_compton{1,c+1}.Del_y, '.');
    colormap (jet (1024));
    hexscatter(num_compton{1,c+1}.z0, num_compton{1,c+1}.Del_y, 'res', 100);
    axis ([-slab_thickness/2 slab_thickness/2 -10 10]);
%     title('\Delta Y vs. Z');
    xlabel('Z location (mm)');
    ylabel('$\Delta$ Y (mm)');
    colorbar
    grid on

    h(4) = figure(4)
%     subplot (2, 2, 3)
%     scatter(num_compton{1,c+1}.z0, num_compton{1,c+1}.Del_z, '.');
    colormap (jet (1024));
    hexscatter(num_compton{1,c+1}.z0, num_compton{1,c+1}.Del_z, 'res', 100);
    axis ([-slab_thickness/2 slab_thickness/2 -20 20]);
%     title('\Delta Z vs. Z');
    xlabel('Z location (mm)');
    ylabel('$\Delta$ Z (mm)');
    colorbar
    grid on
    
    h(5) = figure(5)
%     subplot (2, 2, 4)
%     scatter(num_compton{1,c+1}.z0, num_compton{1,c+1}.Del_d, '.');
    colormap (jet (1024));
    hexscatter(num_compton{1,c+1}.z0, num_compton{1,c+1}.Del_d, 'res', 100);
    axis ([-slab_thickness/2 slab_thickness/2 0 20]);
%     title('\Delta D vs. Z');
    xlabel('Z location (mm)');
    ylabel('$\Delta$ D (mm)');
    colorbar
    grid on
    
    % Paused to view plots (unpause to make faster)
    %pause;
    
    % Save plots as fig files.
    savefig(h,['SD_twosided_compton_' num2str(c)]);
    
    clf;
    
end

h_all(1) = figure(6)
colormap (jet (1024));
hexscatter(inputTable.z0, inputTable.Del_x, 'res', 100);
axis ([-slab_thickness/2 slab_thickness/2 -10 10]);
xlabel('Z location (mm)');
ylabel('$\Delta X$ (mm)');
colorbar
grid on

h_all(2) = figure(7)
colormap (jet (1024));
hexscatter(inputTable.z0, inputTable.Del_y, 'res', 100);
axis ([-slab_thickness/2 slab_thickness/2 -10 10]);
xlabel('Z location (mm)');
ylabel('$\Delta Y$ (mm)');
colorbar
grid on

h_all(3) = figure(8)
colormap (jet (1024));
hexscatter(inputTable.z0, inputTable.Del_z, 'res', 100);
axis ([-slab_thickness/2 slab_thickness/2 -20 20]);
xlabel('Z location (mm)');
ylabel('$\Delta Z$ (mm)');
colorbar
grid on

h_all(4) = figure(9)
colormap (jet (1024));
hexscatter(inputTable.z0, inputTable.Del_d, 'res', 100);
axis ([-slab_thickness/2 slab_thickness/2 0 20]);
xlabel('Z location (mm)');
ylabel('$\Delta X$ (mm)');
colorbar
grid on

savefig(h_all,'SD_twosided_all');

pause;

close all;

end