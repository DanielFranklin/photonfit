function fh = gen_psa_twosided (atten_length, slab_thickness)

fh = @(params, grid_coords) pointsourceattenuative_twosided (params, grid_coords, atten_length, slab_thickness);
