%% Break up input data for processing by event ID.
% Use position = 1 at the beginning of file, then start at position = row
% (output) for each consecutive run.

function [output, row, status] = event_group(filename, position)
tic
fileID = fopen(filename, 'r');

formatSpec = '%f %f %f %f %f %f %f %f %f %f %f';
line = textscan(fileID, formatSpec, 1, 'HeaderLines', position, 'Delimiter',',');
event = line{1,4};

row=1;
cellarray = cell(100000, 11);
cellarray(row,:) = line;

while ~feof(fileID)
    line = textscan(fileID, formatSpec, 1, 'Delimiter',',');
    row = row+1;
    if line{1,4} ~= event
        break
    end
    cellarray(row+1,:) = line;
end

output = cell2mat(cellarray);
status = feof(fileID);

fclose(fileID);
toc
end