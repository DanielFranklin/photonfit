% Parameters are x, y, z of point source, linear attenuation coefficient of
% medium, and original photon yield (source brightness).  Question: can we
% fix the latter and assume it is constant?  This may help distinguishing
% between scattered and pure photoelectric events.

function F = pointsourceattenuative_twosided (params, grid_coords, atten_length, slab_thickness)

atten_co = 1/atten_length; % attenutation coefficient = 1/attenuation length (in mm)

dx = grid_coords (1, 2, 1) - grid_coords (1, 1, 1); % size of pixel (mm) in x dimension.
dy = grid_coords (2, 1, 2) - grid_coords (1, 1, 2); % size of pixel (mm) in y dimension.

dsq = (grid_coords (:, :, 1) - params(1)) .^ 2 + (grid_coords (:, :, 2) - params(2)) .^ 2 + params(3) ^ 2;
% In units of photons/pixel rather than photons/unit area
F1 = params(5) * params(3) .* exp (-sqrt (dsq) * atten_co) ./ (4 * pi * dsq .^ 1.5) * dx * dy;

dsq = (grid_coords (:, :, 1) - params(1)) .^ 2 + (grid_coords (:, :, 2) - params(2)) .^ 2 + (slab_thickness - params(3)) ^ 2;
% In units of photons/pixel rather than photons/unit area
F2 = params(5) * params(3) .* exp (-sqrt (dsq) * atten_co) ./ (4 * pi * dsq .^ 1.5) * dx * dy;

F = [F1 F2];

%imagesc (F);

%pause;
