% Parameters are x, y, z of point source, linear attenuation coefficient of
% medium, and original photon yield (source brightness).  Question: can we
% fix the latter and assume it is constant?  This may help distinguishing
% between scattered and pure photoelectric events.

function F = pointsourceattenuative (params, gridcoords)

atten_co = 1/10; % attenutation coefficient = 1/attenuation length (in mm)
dx = gridcoords (1, 2, 1) - gridcoords (1, 1, 1);
dy = gridcoords (2, 1, 2) - gridcoords (1, 1, 2);

dsq = (gridcoords (:, :, 1) - params(1)) .^ 2 + (gridcoords (:, :, 2) - params(2)) .^ 2 + params(3) ^ 2;
%dsq = (X (:, :, event) - params(1)) .^ 2 + (gridcoords (:, :, 2) - params(2)) .^ 2 + params(3) ^ 2;

%F = params(5) * params(3) ./ (4 * pi * dsq .^ 1.5).* exp (-sqrt (dsq) * atten_co);
%F = params(5) * params(3) ./ (4 * pi * dsq .^ 1.5);% .* exp (-sqrt (dsq) * params(4));

% In units of photons/pixel rather than photons/unit area
F = params(5) * params(3) .* exp (-sqrt (dsq) * atten_co) ./ (4 * pi * dsq .^ 1.5) * dx * dy;

%imagesc (F);

%pause;
