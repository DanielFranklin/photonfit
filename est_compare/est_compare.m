% Compare estimated position of scintillation event between back, front and
% two sided detectors. Input data as matrix.

function est_compare(backMatrix, frontMatrix, twosidedMatrix)

 unique_twosided = unique(twosidedMatrix(:,11:13), 'rows', 'stable');
 unique_back = unique(backMatrix(:,11:13), 'rows');
 unique_front = unique(frontMatrix(:,11:13), 'rows');

 commonrows_twosided_back = ismember(unique_twosided(:,1:3), unique_back(:,1:3), 'rows');
 commonrows_back_twosided = ismember(unique_back(:,1:3), unique_twosided(:,1:3), 'rows');
 
 cr =
 
%  commonrows_back = ismember(backMatrix(:,11:13), twosidedMatrix(:,11:13), 'rows') & ismember(backMatrix(:,11:13), ...
%      frontMatrix(:,11:13), 'rows');
%  
%  commonrows_front = ismember(frontMatrix(:,11:13), twosidedMatrix(:,11:13), 'rows') & ismember(frontMatrix(:,11:13), ...
%      backMatrix(:,11:13), 'rows');
%  
 
 estposition_twosided = twosidedMatrix(commonrows_twosided_back, [2:4 11:13]);
 estposition_back = backMatrix(commonrows_back_twosided, [2:4 11:13]);
 estposition_front = frontMatrix(commonrows_front, [2:4 11:13]);

end
